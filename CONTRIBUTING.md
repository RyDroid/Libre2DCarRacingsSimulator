# Contributing

## Files

### Text file "settings"

A text file is a file readable with a text editor.
For example, files containing only Markdown, TeX, HTML, SVG and source code in C, C++, Go, Java, Scala, Python are considered as text files, but OpenDocument and PDF are not.

All text files have to be in [UTF-8](https://en.wikipedia.org/wiki/UTF-8) (without [BOM](https://en.wikipedia.org/wiki/Byte_order_mark)) encoding (generally UTF-8 on GNU/Linux and [ISO 8859-15](https://en.wikipedia.org/wiki/ISO/IEC_8859-15) on MS Windows) and LF [character newline](https://en.wikipedia.org/wiki/Newline) (sometime called UNIX newline/endline) (Windows generally used CRLF).
Unfortunately, by default some text editors save with the same encoding and character newline of your OS.

An empty line must be at the end of text files.
The aim is readability with [`cat file`](https://en.wikipedia.org/wiki/Cat_%28Unix%29).

#### Configuring text file "settings"

- If your editor manages [EditorConfig](http://editorconfig.org/), you have nothing to do, enjoy!
- [Geany](http://geany.org/) : For creating new files with UTF-8 and LF, go to "Edit" > "Preferences" > "Files" (tab), then choose "Unix (LF)" for line breaks and "UTF-8" as the default encoding.
- [Eclipse](https://eclipse.org/) : In order to force to use UTF-8 and LF for this project, do right click on the project in "Package Explorer", then "Properties", in the right panel select "Resource", now you are on the right and enough intelligent to find what to change.
- [Qt Creator](https://en.wikipedia.org/wiki/Qt_Creator) : For doing the same thing, go to "Tools" > "Options" > "Text Editor" > "Behavior".

### Source code

#### Coding style

We use english, so please use it everywhere in the project (messages, function names, doc, etc).

Names of variables, functions/methods, classes and everything else have to be clear, even if the name is a little longer.
You also do not have to forget to create documentation.

Following the [PEP (Python Enhancement Proposal) 20](https://www.python.org/dev/peps/pep-0020/) is a good thing, even if the project does not use the Python language.

The C code style is the [GNU one](https://www.gnu.org/prep/standards/standards.html#Writing-C).
The C++ code style is also the [GNU one](https://gcc.gnu.org/onlinedocs/libstdc++/manual/source_code_style.html).
Fortunately, many editors can help you (like [GNU Emacs](http://www.emacswiki.org/emacs/IndentingC), Qt Creator and Eclipse).
C99 and C++ short comment ("// comment") should be avoided to ease a potential [C89 / C90](https://en.wikipedia.org/wiki/ANSI_C#C89) port.

#### C/C++ compilation

The project should compile with at least 2 different C/C++ compilers (different does not mean version number).
For example, you can try to compile the project with [GCC](https://gcc.gnu.org/) and [Clang](http://clang.llvm.org/), that are free/libre, gratis and work on various OS (including GNU/Linux, *BSD, Windows and OS X).

##### C compilation

At least some source files in C need a [C99](https://en.wikipedia.org/wiki/C99) or [C11](https://en.wikipedia.org/wiki/C11_%28C_standard_revision%29) compiler.
A C++ compiler may do the job, if you have not a C99 or C11 compiler installed.

##### C++ compilation

At least some source files in C++ need a [C++11](https://en.wikipedia.org/wiki/C%2B%2B11) or [C++14](https://en.wikipedia.org/wiki/C%2B%2B14) compiler.
C++ source code that is compatible with version x has to be also compatible with newer version of C++.

#### Names of files

Headers in C++ must end with hpp and implementation with cpp.
Like that, there is no confusion between C and C++.
It is better for many reasons, like being able to do a C++ wrapper with the same name except the extension and not confusing some editors with syntax highlighting.

### Formatted text files

If you need to make a structured document, you should consider [Markdown](https://en.wikipedia.org/wiki/Markdown).
For example, this document uses the Markdown syntax.

For longer texts or presentations, [LaTeX](http://latex-project.org/) and HTML/CSS(/JS) could be good options.

#### Markdown

A list item must be done with "-" (minus character) to avoid potential confusion with "*" that can be used for emphasizing.
The extension of Markdown files must be "md" or "markdown", "mkd" is not widely recognized as Markdown so it must not be used.

## Licensing policy

You must release your contributions under free/libre license(s) if you want them to be accepted [upstream](https://en.wikipedia.org/wiki/Upstream_%28software_development%29).

## Git

### Installation

#### Debian

The above text should also work on derivatives of [Debian](https://www.debian.org/), like [Trisquel](https://trisquel.info/), [gNewSense](http://www.gnewsense.org/), Ubuntu and Mint.

You have to install the git package and its dependencies.
You must have [SuperUser/root](https://en.wikipedia.org/wiki/Superuser) rights, that you can have with `su` or `sudo command`.
You can use `apt-get install`, `aptitude install` (if [aptitude](https://wiki.debian.org/Aptitude) is installed) or a graphical user interface with [Synaptic](https://wiki.debian.org/Synaptic) (if it is installed).

#### Fedora, CentOS and RHEL

You have to install the git package and its dependencies.
With SuperUser/root rights, you can use `yum install` or a graphical user interface with [Yum Extender](http://www.yumex.dk/) (if it is installed).
[On Fedora version 22 and more, yum is apparently dead](http://dnf.baseurl.org/2015/05/11/yum-is-dead-long-live-dnf/).

#### Microsoft Windows (and ReactOS?) and Apple OS X

Download it on [the official website](http://www.git-scm.com/downloads) and install it.

### Use

If you do not know how to use it, you must read a documentation (like [the official one](http://www.git-scm.com/doc)).

Use `git status` to check that your modifications will be commited.
You have to pull before push.

git provides only a text shell interface.
However, you can install a graphical user interface for it (like [git-cola](https://git-cola.github.io/) or [gitg](https://wiki.gnome.org/Apps/Gitg/)).

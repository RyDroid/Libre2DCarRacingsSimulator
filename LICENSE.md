# Licensing policy

It is [free/libre software](https://www.gnu.org/philosophy/free-sw.html), media (including images, sounds and videos) and documentation.
Generally a license is specified as a header of a file.
If there is no information about the license in the header, the license must be considered to be [AGPLv3.0](https://www.gnu.org/licenses/agpl.html)+, except for files with less than 10 lines that are under [Creative Commons 0 v1.0](https://creativecommons.org/publicdomain/zero/1.0/).

## Documentation

If there is no information about the license in a header source file of documentation and that the file has more than 10 lines, it is not only under AGPLv3.0+, it is also under [GFDL](https://www.gnu.org/copyleft/fdl.html)v1.3, [Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) 4.0 and [FAL](http://artlibre.org/licence/lal/)v1.3.
You can choose the license you wish among those mentioned.
It is also true for the version if multiple versions are proposed for a given license.

Markdown and LaTeX files are covered by this section.

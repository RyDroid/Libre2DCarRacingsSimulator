# A free/libre 2D simulator of car racings

## Contribute

See [CONTRIBUTING.md](CONTRIBUTING.md).

## License and authors

See [LICENSE.md](LICENSE.md) and logs of git for the full list of contributors of the project.

## Frequently Asked Questions

- Why nearly all C source code is in *.h?
  I first would like to have something that works before doing a "good compilation".
  Moreover, this is conveniant for lazy creators of drivers, because they do not need to compile header files.
  You need to have a [C99](https://en.wikipedia.org/wiki/C99) or [C11](https://en.wikipedia.org/wiki/C11_%28C_standard_revision%29) compiler to use them, you can use option -std=c99 or std=c11 for [GCC](https://gcc.gnu.org/) and [Clang](http://clang.llvm.org/).
  It is not needed to copy-paste them in your project, you can use option -Ipath of GCC and Clang.
- What about a script that creates a static library with C source code for lazy creators of drivers?
  We appreciate your help.
- What about using [C++](https://en.wikipedia.org/wiki/C%2B%2B), [Go](https://golang.org/) or [Rust](https://www.rust-lang.org/)?
  It would be easier.
  Unfortunately, this project had to be done in C only (it was a school project).
  New students may have the same subject with the same rule "C only", so C source code must not be replaced by an other language (but wrappers can be made) and C has to remain the prefered language if the code can be used for a driver.
- Is the simulator working?
  No.
  There is a simulator that works, but it is not [free/libre](https://www.gnu.org/philosophy/free-sw.html).
  The original aim of this project was to rewrite piece by piece the [proprietary](https://www.gnu.org/philosophy/proprietary.html) simulator with free/libre code mainly in C.
  Due to copyright issues, it may not be legal to share the proprietary simulator, but a old version may still be avaible on [the website of Julien Gosme](http://www.gosme.org/GrandPrix.html) (that released the first versions) (unfortunately without information about the license, so by default it is proprietary in most juridictions), however if you are a student at [ENSICAEN](http://www.ensicaen.fr/) (in France) there should be no problem.
  You may consider that the "proprietary simulator" is [private software](https://www.gnu.org/philosophy/categories.html#PrivateSoftware), because students at ENSICAEN have the source code and can modify it, moreover it was not intended to be used outsite ENSICAEN.
  At least in 2015, Nicola Spanti did not suffer from problem(s) to [fork](https://en.wikipedia.org/wiki/Fork_%28software_development%29) the simulator and say it publicly.
  Unfortunately, there was no explicit right to do it and it may be still true, so it may not remain tolerated in the future.
- I do not have a png directory with a needed header, how can I get it?
  You need free/libre [libpng++](http://www.nongnu.org/pngpp/).
  There is a package for at least [Debian](https://packages.debian.org/stable/libpng++-dev) and [Ubuntu](http://packages.ubuntu.com/trusty/libpng++-dev).

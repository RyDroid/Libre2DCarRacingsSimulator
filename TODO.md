# Things to do

* Creating a first version of the simulator...

## Bugs

Currently none is known!

## Improvements

### Source code

#### Interfaces for human-beings

* Creating a text shell interface
* Creating a [GTK+](http://www.gtk.org/) and/or [Qt](https://www.qt.io/) interface
* Creating an Android port
* Adding a way to pause the simulation

##### map-2d-info

* Add actions including:
  * car-starting-positions
  * nb-max-drivers
  * is-valid (all cars can reach a goal + there is at least one starting point + there is no invalid characters)
  * summary (see loadTrack that should be removed)

##### map-2d-convert

* Add options
  * -i --input input-file-path (optionnal but usefull for listing args without order)
  * -o --output input-file-path (same as -i but for output)
  * --stdin reads data from stdin instead of a regular file
  * --stdout prints data to stdout instead of a regular file
  * --no-check does not do check to the input file (good for speed)
  * --input-type type (like png, txt/text)
  * --output-type type (like png, txt/text)
* Detect input file names that are obviously bad (.jpg, .jpeg, etc)
* Add [PPM (Portable PixMap)](https://en.wikipedia.org/wiki/Netpbm_format) support

##### make-tracks

* Arguments could be passed to converter (this could be usefull for fuel options)
* Use threads or processes but queueing warning and error messages
  * Rewrite in an other language different of POSIX shell will probably be needed.
* Rewrite in a more portable language for non POSIX systems (ReactOS, Windows, etc)

#### Driver "API"

* Tell to drivers theirs angles at least at beginning (but this may break some drivers)
* Allow to do a race with 1 to ten cars ('0' character is first and '9' character is tenth) (but this may break some drivers and maps must be adapted to support more or less than 3 drivers)

#### Others

* Explain how to create a driver (what information the simulator sends and add some potential problems)
* When syntax is invalid, print what the driver writes to stdout
* Creating a shared library for avoiding duplication with the simulator and map2d-info
* Creating packages (deb, rpm, etc)
* Creating wrappers for some C functions
  * C++
  * Objective-C
  * C# (C-sharp)
  * Java
* Put drivers in a sandbox (limit memory, ps -ef | grep otherDriver | awk only-pid -> kill pids, etc)

### Others

* Creating a logo

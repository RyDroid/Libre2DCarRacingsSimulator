# Creating a driver

## Avoiding traps

- Your car may be in front of a wall, in this case you have to turn your car.
- Some maps can be big, so a best path algorithm finder may take too much time.
  You have to detect to use an other algortihm.

## Optimizations

- If your compiler has options for optimizing use them.
  With GCC and Clang, you can use -O3 for very aggresive optimizations.
- There may be some goals that are not reachable.
  If you try to find the fastest path, it would be useless to try to go to them.
  Some may be obvious to detect if they have only goal and out tiles around them.
- You can use threads (with a C11 library or pthread on a POSIX OS).

#!/bin/sh

# Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Automatic variables could be used instead.
# Unfortunately it is specific to GNU make.
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html

converter=bin/map-2d-converter

if test ! -e $converter
then
    echo "$converter does not exist!" 1>&2
    exit 1
fi
if test ! -x $converter
then
    echo "$converter exists but is not executable!" 1>&2
    exit 1
fi

nb_tracks=0
nb_conversions=0
for track_png in tracks/*.png
do
    nb_tracks=$(($nb_tracks +1))
    track_txt=$(echo $track_png | sed 's/.png/.txt/g')
    if test ! -e $track_txt
    then
	nb_conversions=$(($nb_conversions +1))
	echo "track-converter $track_png $track_txt"
	$converter --png2txt $track_png $track_txt
    fi
done

if test $nb_tracks -eq 0
then
    echo 'No track found!' 1>&2
fi

# Copyright (C) 2015-2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


SRC_DIR=src
INC_DIR=src
OBJ_DIR=obj
BIN_DIR=bin
DOC_DIR=doc

CC=gcc
CXX=g++
INCLUDES=-I$(INC_DIR) -Isrc/utils/ -Isrc/specific/
DEBUG_FLAGS=-D_FORTIFY_SOURCE=0 -O0 -g -DDEBUG
C_CXX_FLAGS=-Wall -Wextra -Wpedantic -Wformat -Werror \
	-Wno-unused-command-line-argument \
	-Wl,-z,relro -Wl,-z,now \
	-fPIC -fPIE -pie \
	-O2 -s \
	$(INCLUDES) $(DEBUG_FLAGS)
C_FLAGS=$(C_CXX_FLAGS) -std=c99
CXX_FLAGS=$(C_CXX_FLAGS) -std=c++11
LIBS=-lm
RM=rm -f

PACKAGE=2d-car-racings-simulator
VERSION=` date +"%Y-%m-%d" `
ARCHIVE=$(PACKAGE)_$(VERSION)
FILES_TO_ARCHIVE=$(SRC_DIR)/ $(INC_DIR)/ makefile *.sh \
	*.md licenses/ \
	.gitignore .editorconfig .dir-locals.el .gitlab-ci.yml


.PHONY: \
	tracks \
	archives default-archive zip tar-gz tar-bz2 tar-xz 7z \
	clean clean-bin clean-objects clean-profiling clean-simulator \
	clean-tmp clean-archives clean-latex


all: bin tracks default-archive


tracks: tracks-source tracks-convert

tracks-convert: map-2d-converter
	@chmod +x make-tracks.sh
	./make-tracks.sh

tracks-source:
	git submodule init   tracks
	git submodule update tracks


bin: map-2d-info map-2d-converter

map-2d-info: $(BIN_DIR)/map-2d-info

$(BIN_DIR)/map-2d-info: $(OBJ_DIR)/map-2d-info.o $(OBJ_DIR)/tab_2d_char_plus.o
	@mkdir -p $(BIN_DIR)
	$(CXX) $(CXX_FLAGS) \
		$(OBJ_DIR)/map-2d-info.o      \
		$(OBJ_DIR)/tab_2d_char_plus.o \
		-o $(BIN_DIR)/map-2d-info     \
		`libpng-config --ldflags`

$(OBJ_DIR)/map-2d-info.o: $(SRC_DIR)/specific/map-2d-info.cpp $(INC_DIR)/utils/*.h $(INC_DIR)/utils/*.hpp $(INC_DIR)/specific/*.h $(INC_DIR)/specific/*.hpp
	@mkdir -p $(OBJ_DIR)
	$(CXX) $(CXX_FLAGS) \
		-c $(SRC_DIR)/specific/map-2d-info.cpp \
		-o $(OBJ_DIR)/map-2d-info.o            \
		`libpng-config --cflags`

map-2d-converter: $(BIN_DIR)/map-2d-converter

$(BIN_DIR)/map-2d-converter: $(OBJ_DIR)/map-2d-converter.o
	@mkdir -p $(BIN_DIR)
	$(CXX) $(CXX_FLAGS) \
		$(OBJ_DIR)/map-2d-converter.o  \
		-o $(BIN_DIR)/map-2d-converter \
		`libpng-config --ldflags`

$(OBJ_DIR)/map-2d-converter.o: \
		$(SRC_DIR)/specific/map-2d-converter.cpp \
		$(INC_DIR)/utils/*.h $(INC_DIR)/utils/*.hpp \
		$(INC_DIR)/specific/*.h $(INC_DIR)/specific/*.hpp
	@mkdir -p $(OBJ_DIR)
	$(CXX) $(CXX_FLAGS) \
		-c $(SRC_DIR)/specific/map-2d-converter.cpp \
		-o $(OBJ_DIR)/map-2d-converter.o \
		`libpng-config --cflags`

$(OBJ_DIR)/tab_2d_char_plus.o: \
		$(SRC_DIR)/utils/tab_2d_char_plus.c $(INC_DIR)/utils/tab_2d_char_plus.h \
		$(INC_DIR)/utils/tab_2d_char_essential.h $(INC_DIR)/utils/tab_2d_generic_static.h \
		$(INC_DIR)/utils/bool.h
	@mkdir -p $(OBJ_DIR)
	$(CC) $(C_FLAGS) \
		-c $(SRC_DIR)/utils/tab_2d_char_plus.c \
		-o $(OBJ_DIR)/tab_2d_char_plus.o

$(OBJ_DIR)/Map2D.o: \
		$(SRC_DIR)/specific/Map2D.cpp $(SRC_DIR)/specific/Map2D.hpp \
		$(SRC_DIR)/specific/map_2d_tile.h $(SRC_DIR)/specific/map_tile.h \
		$(SRC_DIR)/utils/tab_2d_char_io.hpp $(INC_DIR)/utils/tab_2d_char_io.h \
		$(INC_DIR)/utils/tab_2d_char_plus.h $(INC_DIR)/utils/tab_2d_char_essential.h $(INC_DIR)/utils/tab_2d_generic_static.h \
		$(INC_DIR)/utils/bool.h
	@mkdir -p $(OBJ_DIR)
	$(CXX) $(CXX_FLAGS) \
		-c $(SRC_DIR)/specific/Map2D.cpp \
		-o $(OBJ_DIR)/Map2D.o


archives: zip tar-gz tar-bz2 tar-xz 7z

default-archive: tar-xz

zip: $(FILES_TO_ARCHIVE)
	zip $(ARCHIVE).zip -r -- $(FILES_TO_ARCHIVE) > /dev/null

tar-gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(ARCHIVE).tar.gz -- $(FILES_TO_ARCHIVE) > /dev/null

tar-bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(ARCHIVE).tar.bz2 -- $(FILES_TO_ARCHIVE) > /dev/null

tar-xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(ARCHIVE).tar.xz -- $(FILES_TO_ARCHIVE) > /dev/null

7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(ARCHIVE).7z $(FILES_TO_ARCHIVE) > /dev/null


clean: clean-bin clean-objects clean-profiling clean-doc \
	clean-tmp clean-archives clean-latex clean-tracks clean-ide clean-python
	$(RM) -rf -- \
		*.elc \
		.*rc .bash* .zsh* .R* .octave* .xsession*
	cd $(SRC_DIR) && make --directory=.. clean-bin clean-tmp

clean-bin:
	$(RM) -rf -- \
		$(BIN_DIR) drivers/ \
		*.o *.a *.so *.ko *.dll *.out

clean-objects:
	$(RM) -rf -- $(OBJ_DIR)

clean-profiling:
	$(RM) -rf -- callgrind.out.*

clean-doc:
	$(RM) -rf -- doc/ documentation/

clean-tmp:
	$(RM) -rf -- \
		*~ .\#* \#* *.bak *.backup *.sav *.save *.autosav *.autosave \
		*.log *.log.* log/ logs/ \
		.cache/ .thumbnails/

clean-archives:
	$(RM) -f -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk *.ipa \
		*.zip *.tar.* *.tgz *.7z *.gz *.bz2 *.lz *.lzma *.xz *.rar *.jar

clean-latex:
	$(RM) -f -- \
		*.pdf *.dvi *.ps \
		*.aux *.cut *.nav *.toc *.fls *.snm *.vrb *.vrm *.fdb_latexmk *.synctex *.synctex.gz *-converted-to.*

clean-tracks:
	cd tracks && make clean

clean-ide:
	$(RM) -f -- qmake_makefile *.pro.user

clean-python:
	$(RM) -rf -- *.pyc __pycache__

clean-git:
	git clean -fdx

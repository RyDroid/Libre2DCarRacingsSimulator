/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions for a driver.
 */


#ifndef DRIVER_H
#define DRIVER_H


#include "cstring_functions.h"
#include "timer.h"
#include "log.h"
#include "debug.h"


/**
 * @brief Reads positions of cars.
 * @param cars Cars to read positions
 * @return Number of readed positions
 */
static inline
unsigned char
read_positions_of_cars_unsafe(table_of_cars* cars
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
			      , bool in_simulator
#endif
			      )
{
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
  if(!in_simulator)
    {
      fprintf(stdout, "Cars:\n");
      table_of_cars_to_stream_as_an_ordered_list_unsafe(cars,
							stdout);
      fprintf(stdout, "\nPositions (%hhu): ", cars->size);
    }
#endif
  
  unsigned char i = 0;
  while(fscanf(stdin, "%u %u",
	       &cars->elements[i].position.x,
	       &cars->elements[i].position.y)
	== 2 &&
	i < cars->size)
    {
      ++i;
      if(fgetc(stdin) == '\n') /* space, \t, \n or \r */
	{
	  break;
	}
    }
  return i;
}

/**
 * @brief Reads positions of cars and set their speeds.
 * Computing speeds of other cars does not work well with random and sequential mode, but is works with determinist mode.
 * @param cars Cars to read positions
 */
static inline
unsigned char
read_positions_of_cars_and_set_speeds_unsafe(table_of_cars* cars
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
					     , bool in_simulator
#endif
					     )
{
  unsigned char i, nb_positions_readed;
  car_position old_positions[cars->size];
  
  for(i=0; i < cars->size; ++i)
    {
      old_positions[i] = cars->elements[i].position;
    }
  
  nb_positions_readed = read_positions_of_cars_unsafe(cars
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
						      , in_simulator
#endif
						      );
  
  for(i=0; i < nb_positions_readed; ++i)
    {
      car_define_speed_with_previous_position_unsafe(&cars->elements[i],
						     &old_positions[i]);
    }

  return nb_positions_readed;
}

/**
 * @brief Validates a speedup to the simulator.
 * @param speedup Speedup to validate
 */
static inline
bool
driver_validate_turn_speedup_unsafe(const car_speedup* speedup
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
				    , bool in_simulator
#endif
				    )
{
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
  if(!in_simulator)
    {
      fputs("Speedup defined: ", stdout);
    }
#endif
  
  car_speedup_validate_turn_to_stream_unsafe(speedup, stdout);
  if(fflush(stdout) == EOF)
    {
      fprintf(stderr, "An error occurred while validating speedup!\n");
      if(fflush(stdout) == EOF)
	{
	  fprintf(stderr, "It failed again while validating speedup!\n");
	  return false;
	}
      fflush(stderr);
    }
  return true;
}

/**
 * @brief Defines speedup of the managed car for this turn.
 * @param map Map and cars of the race
 * @param speedup Speedup to validate
 */
static inline
bool
set_speedup_of_managed_car_unsafe(gp_map* map,
				  const car_speedup* speedup
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
				  , bool in_simulator
#endif
				  )
{
  bool res;
  car* managed_car = &map->cars.elements[DEFAULT_MANAGED_CAR_INDEX];
  res = car_consume_fuel_by_moving(managed_car,
				   speedup,
				   map_2d_tile_get_element_value_unsafe(&map->map, managed_car->position.x, managed_car->position.y));
  res = res && driver_validate_turn_speedup_unsafe(speedup
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
						   , in_simulator
#endif
						   );
  return res;
}

/**
 * @brief Returns the stream to read the map.
 * @return The stream to read the map
 */
static inline
FILE*
get_map_stream(
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
     int argc, char* argv[]
#endif
     )
{
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
  if(argc > 2 &&
     (cstring_equals(argv[1], "-g") ||
      cstring_equals(argv[1], "--dbg") ||
      cstring_equals(argv[1], "--debug")
      ))
    {
      FILE* map_stream = fopen(argv[2], "r");
      return map_stream == NULL
	? stdin : map_stream;
    }
#endif
  
  return stdin;
}


#endif

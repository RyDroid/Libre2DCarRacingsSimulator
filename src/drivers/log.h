/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions for logging a driver activity.
 */


#ifndef LOG_H
#define LOG_H


#include "gp_map_io.h"


/**
 * @brief Put arguments to a stream.
 * @param argc Number of arguments (>= 1)
 * @param argv Arguments
 * @param stream A not null stream to put arguments
 */
static inline
void
cstrings_arguments_to_stream_in_markdown_unsafe(int argc,
						char* argv[],
						FILE* stream)
{
  fprintf(stream, "## Arguments (%d)\n\n", argc);
  for(int i=0; i < argc; ++i)
    {
      fputs(argv[i], stream);
      fputc('\n', stream);
    }
}

/**
 * @brief Put arguments to a stream.
 * @param argc Number of arguments (>= 1)
 * @param argv Arguments
 * @param stream A stream to put arguments
 */
static inline
void
cstrings_arguments_to_stream_in_markdown(int argc,
					 char* argv[],
					 FILE* stream)
{
  if(argv != NULL && stream != NULL)
    {
      cstrings_arguments_to_stream_in_markdown_unsafe(argc, argv, stream);
    }
}

/**
 * @brief Prints information about a map and cars in markdown to stream.
 * @param map_and_cars A not null pointer on a 2D map and cars of a racing
 * @param stream A not null pointer on a stream to print in
 */
static inline
void
gp_map_to_stream_in_markdown_unsafe(const gp_map* map_and_cars,
				    FILE* stream)
{
  fputs("## Map and cars", stream);
  fputc('\n', stream);
  fputc('\n', stream);
  
  fprintf(stream, "### Cars (%u)", map_and_cars->cars.size);
  fputc('\n', stream);
  if(map_and_cars->cars.size == 0)
    {
      fputs("Starters were removed from the map.", stream);
    }
  else
    {
      gp_map_print_cars_to_stream_as_an_ordered_list(map_and_cars, stream);
    }
  fputc('\n', stream);
  fputc('\n', stream);
  
  fputs("### Map", stream);
  fputc('\n', stream);
  fprintf(stream,
	  "nbLines=%u, nbColumns=%u\n",
	  map_and_cars->map.nb_lines,
	  map_and_cars->map.nb_columns);
  gp_map_print_map_to_stream_without_grid(map_and_cars, stream);
  fputc('\n', stream);
  fputc('\n', stream);
}

/**
 * @brief Prints information about a map and cars in markdown to stream.
 * @param map_and_cars 2D map and cars of a racing
 * @param stream A stream to print in
 * @return True if it successeds, otherwise false
 */
static inline
bool
gp_map_to_stream_in_markdown(const gp_map* map_and_cars,
			     FILE* stream)
{
  if(map_and_cars != NULL && stream != NULL)
    {
      gp_map_to_stream_in_markdown_unsafe(map_and_cars, stream);
      return true;
    }
  return false;
}


#endif

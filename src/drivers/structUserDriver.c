/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */



/* Macro DEBUG can be defined with #define, but also with -DDEBUG with GCC and Clang. */
#ifdef DEBUG
#define DEBUG_CURRENT_LEVEL DEBUG_LEVEL_HIGH
#else
#define DEBUG_CURRENT_LEVEL DEBUG_LEVEL_NONE
#endif


#include "driver.h"



int
main(
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_MEDIUM
     int argc, char* argv[]
#endif
     )
{
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_MEDIUM
  timer time = get_timer_now();
  
  unsigned int log_file_path_length = strlen(argv[0] + 5);
  // 5 = strlen(".log") + 1 ('\0')
  char log_file_path[log_file_path_length];
  strcpy(log_file_path, argv[0]);
  strcat(log_file_path, ".log");
  FILE* log_file = fopen(log_file_path, "w");
  if(log_file == NULL)
    {
      fprintf(stderr, "Failed to open debugging file!\n");
    }
  fprintf(log_file, "# Information about a race\n\n");

#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
  if(log_file != NULL)
    {
      cstrings_arguments_to_stream_in_markdown_unsafe(argc, argv, log_file);
      fputc('\n', log_file);
    }
#endif
#endif

  FILE* map_stream = get_map_stream(
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
				  argc, argv
#endif
				    );
#define in_simulator (map_stream == stdin)
  
  gp_map map_and_cars = gp_map_create_empty();
  if(!gp_map_load_from_stream_unsafe(&map_and_cars, map_stream, true))
    {
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_LOW
      fprintf(log_file, "Error while loading the map!\n");
#endif
      return EXIT_FAILURE;
    }
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
  if(map_stream != stdin)
    {
      fclose(map_stream);
    }
#endif
  
  unsigned int turn = 0;
  car_speedup speedup;
  
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_MEDIUM
  if(log_file != NULL)
    {
      gp_map_to_stream_in_markdown_unsafe(&map_and_cars, log_file);

      fprintf(log_file, "### Time to initialize\n");
      fprintf(log_file, "%lf s", get_nb_seconds_between_timer_and_now(time));
      fputc('\n', log_file);
      fputc('\n', log_file);
  
      fprintf(log_file, "## The race\n");
    }
#endif
  
  while(!feof(stdin))
    {
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_MEDIUM
      time = get_timer_now();
#endif
      
      ++turn;
      
      if(turn == 1)
	{
	  read_positions_of_cars_unsafe(&map_and_cars.cars
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
					, in_simulator
#endif
					);
	}
      else
	{
	  read_positions_of_cars_and_set_speeds_unsafe(&map_and_cars.cars
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
						       , in_simulator
#endif
						       );
	}
      
      /* Defining speedup */
#define managed_car map_and_cars.cars.elements[MANAGED_CAR_INDEX]
      speedup.x =  0;
      speedup.y = -1;
      set_speedup_of_managed_car_unsafe(&map_and_cars, &speedup
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_HIGH
					, in_simulator
#endif
					);
      
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_MEDIUM
      if(log_file != NULL)
	{
	  fprintf(log_file, "\n### Turn %u\n", turn);
	  
	  fprintf(log_file,
		  "#### Cars (me=%hhu)\n",
		  (unsigned char)(DEFAULT_MANAGED_CAR_INDEX +1));
	  gp_map_print_cars_to_stream_as_an_ordered_list(&map_and_cars,
							 log_file);

	  fprintf(log_file, "\n#### Action\n");
	  fprintf(log_file, "Speedup = (x=%hhd, y=%hhd)\n",
		  speedup.x, speedup.y);
	  
	  fprintf(log_file, "#### Other information\n");
	  fprintf(log_file, "Time = %lf s\n",
		  get_nb_seconds_between_timer_and_now(time));
	  
	  if(fflush(log_file) == EOF)
	    {
	      fprintf(stderr, "An error occurred while flushing log file!\n");
	    }
	}
#endif
    }
  
#if DEBUG_CURRENT_LEVEL >= DEBUG_LEVEL_MEDIUM
  fclose(log_file);
#endif
  gp_map_destruct_unsafe(&map_and_cars);
  return EXIT_SUCCESS;
}

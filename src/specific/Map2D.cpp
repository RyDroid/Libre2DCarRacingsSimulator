/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "Map2D.hpp"


namespace CarRacings
{
  Map2D::Map2D()
    : map(tab_2d_char_create_empty())
  {}
  
  Map2D::Map2D(unsigned int nbLines, unsigned int nbColumns)
    : map(tab_2d_char_create_with_null_values(nbLines, nbColumns))
  {}
  
  Map2D::Map2D(const CarRacings::Map2D& map)
  {
    this->map = tab_2d_char_get_copy(&map.map);
  }
  
#if __cplusplus >= 201103L
  Map2D::Map2D(CarRacings::Map2D&& map)
  {
    this->map.nb_lines   = map.map.nb_lines;
    this->map.nb_columns = map.map.nb_columns;
    
    this->map.tab = map.map.tab;
    map.map.tab = nullptr;
  }
#endif
  
  Map2D::~Map2D()
  {
    tab_2d_char_destruct_unsafe(&this->map);
  }
  
  
  CarRacings::Map2D&
  Map2D::operator=(const CarRacings::Map2D& map)
  {
    if(&this->map != &map.map)
      {
        this->map = tab_2d_char_get_copy(&map.map);
      }
    return *this;
  }
  
#if __cplusplus >= 201103L
  CarRacings::Map2D&
  Map2D::operator=(CarRacings::Map2D&& map)
  {
    if(&this->map != &map.map)
      {
	this->map.nb_lines   = map.map.nb_lines;
	this->map.nb_columns = map.map.nb_columns;
	
	free(this->map.tab);
	this->map.tab = map.map.tab;
	map.map.tab = nullptr;
      }
    return *this;
  }
#endif
  
  
  void
  Map2D::reserve()
  {
    tab_2d_char_free_unsafe(&this->map);
    tab_2d_char_alloc_null_unsafe(&this->map);
  }
  
  void
  Map2D::reserve(unsigned int nbLines, unsigned int nbColumns)
  {
    tab_2d_char_init_null_unsafe(&this->map, nbLines, nbColumns);
  }
  
  unsigned int
  Map2D::getNbLines()   const
  {
    return this->map.nb_lines;
  }
  
  unsigned int
  Map2D::getNbColumns() const
  {
    return this->map.nb_columns;
  }
  
  void
  Map2D::setNbLines(unsigned int value)
  {
    tab_2d_char_set_nb_lines_unsafe(&this->map, value);
  }
  
  void
  Map2D::setNbColumns(unsigned int value)
  {
    tab_2d_char_set_nb_columns_unsafe(&this->map, value);
  }
  
  map_tile
  Map2D::getElementValue(unsigned int line,
			 unsigned int column) const
  {
    return map_2d_tile_get_element_value_unsafe(&this->map,
						line, column);
  }
  
  void
  Map2D::setElementValue(unsigned int line,
			 unsigned int column,
			 map_tile new_value)
  {
    return tab_2d_char_set_element_value(&this->map,
					 line, column,
					 new_value);
  }
  
  bool
  Map2D::isPositionPossibleForACar(unsigned int line,
				   unsigned int column) const
  {
    return map_2d_tile_is_element_possible_for_a_car(&this->map,
						     line,
						     column);
  }
  
  
  std::ostream&
  operator<<(std::ostream& stream, const CarRacings::Map2D& map)
  {
    return stream << static_cast<tab_2d_char>(map.map);
  }
  
  bool
  Map2D::print(FILE* stream) const
  {
    return tab_2d_char_print_stream_without_grid(&this->map, stream);
  }
}

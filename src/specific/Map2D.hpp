/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief A class to manage a map of a 2D car racing.
 */


#ifndef CAR_RACINGS_MAP_2D_HPP
#define CAR_RACINGS_MAP_2D_HPP


extern "C"
{
#include "map_2d_tile.h"
#include "tab_2d_char_plus.h"
#include "tab_2d_char_io.h"
}
#include "tab_2d_char_io.hpp"


namespace CarRacings
{
  class Map2D
  {
  public:
    Map2D();
    Map2D(unsigned int nbLines, unsigned int nbColumns);
    Map2D(const CarRacings::Map2D& map);
#if __cplusplus >= 201103L
    Map2D(CarRacings::Map2D&& map);
#endif
    ~Map2D();
    
    Map2D& operator=(const CarRacings::Map2D& map);
#if __cplusplus >= 201103L
    Map2D& operator=(CarRacings::Map2D&& map);
#endif
    
    void reserve();
    void reserve(unsigned int nbLines, unsigned int nbColumns);
    unsigned int getNbLines()   const;
    unsigned int getNbColumns() const;
    void setNbLines  (unsigned int value);
    void setNbColumns(unsigned int value);
    map_tile getElementValue  (unsigned int line, unsigned int column) const;
    void     setElementValue  (unsigned int line, unsigned int column, map_tile new_value);
    bool isPositionPossibleForACar(unsigned int line, unsigned int column) const;
    
    friend std::ostream& operator<<(std::ostream& stream, const CarRacings::Map2D& map);
    bool print(FILE* stream = stdout) const;
    
  private:
    map_2d_char map;
  };
}


#endif

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief A structure and functions to manage a car
 */


#ifndef CAR_H
#define CAR_H


#include "position_2d_uint.h"
#include "map_tile.h"
#include "math_macros.h"


typedef signed char signed_char;
DEFINE_POSITION_2D_STRUCT(signed_char)
typedef position_2d_uint car_position;
typedef position_2d_signed_char car_speed;
typedef position_2d_signed_char car_speedup;


#define NB_BOOSTS_DEFAULT         5
#define DEFAULT_MANAGED_CAR_INDEX 0


/**
 * @brief Calcul de la variation de carburant en fonction de la vitesse et de l'accéleration.
 */
static inline
unsigned int
fuel_consumed_with_a_move(int speed_x,
			  int speed_y,
			  int speedup_x,
			  int speedup_y)
{
  return
    POW2(speedup_x) + POW2(speedup_y) +
    (unsigned int)(sqrt(POW2(speed_x) + POW2(speed_y)) * 1.5);
}

static inline
unsigned int
fuel_consumed_with_a_move_in_an_element(int speed_x,
					int speed_y,
					int speedup_x,
					int speedup_y,
					map_tile element)
{
  return
    element == MAP_TILE_OUT
    ? fuel_consumed_with_a_move(speed_x, speed_y, speedup_x, speedup_y) + 1
    : fuel_consumed_with_a_move(speed_x, speed_y, speedup_x, speedup_y);
}

static inline
unsigned int
fuel_consumed_with_a_move_in_an_element_with_structs_unsafe(const car_speed* speed,
							    const car_speedup* speedup,
							    map_tile element)
{
  return
    fuel_consumed_with_a_move_in_an_element(speed->x,
					    speed->y,
					    speedup->x,
					    speedup->y,
					    element);
}


/**
 * @brief A structure to store data of a car.
 */
struct car
{
  car_position position;
  unsigned int fuel;
  unsigned int nb_boosts;
  car_speed speed;
};

typedef struct car car;


/**
 * @brief Creates a car with a defined speedup.
 */
static inline
car
car_create_with_speedup(unsigned int position_x,
			unsigned int position_y,
			unsigned int fuel,
			unsigned int nb_boosts,
			unsigned int speedup_x,
			unsigned int speedup_y)
{
  car created_car;
  created_car.position.x = position_x;
  created_car.position.y = position_y;
  created_car.fuel = fuel;
  created_car.nb_boosts = nb_boosts;
  created_car.speed.x = speedup_x;
  created_car.speed.y = speedup_y;
  return created_car;
}

/**
 * @brief Creates a car with struct parameters.
 */
static inline
car
car_create_with_structs(const car_position* position,
			unsigned int fuel,
			unsigned int nb_boosts,
			const car_speedup* speedup)
{
  return car_create_with_speedup(position->x,
				 position->y,
				 fuel,
				 nb_boosts,
				 speedup->x,
				 speedup->y);
}

/**
 * @brief Creates a car.
 */
static inline
car
car_create(unsigned int position_x,
           unsigned int position_y,
           unsigned int fuel,
           unsigned int nb_boosts)
{
  return car_create_with_speedup(position_x,
				 position_y,
				 fuel,
				 nb_boosts,
				 0, 0);
}

/**
 * @brief Creates a car with default values.
 */
static inline
car
car_create_default(unsigned int position_x,
		   unsigned int position_y,
		   unsigned int fuel)
{
  return car_create(position_x,
		    position_y,
		    fuel,
		    NB_BOOSTS_DEFAULT);
}

/**
 * @brief Makes a car consuming fuel.
 * @param a_car A car
 * @param fuel Quantity of fuel that the car has to consume
 * @return If the given quantity of fuel was consumed it returns true, otherwise false.
 */
static inline
bool
car_consume_fuel(car* a_car,
		 unsigned int fuel)
{
  if(fuel <= a_car->fuel)
    {
      a_car->fuel -= fuel;
      return true;
    }
  a_car->fuel = 0;
  return false;
}

static inline
bool
car_consume_fuel_by_moving(car* a_car,
			   const car_speedup* speedup,
			   map_tile current_tile)
{
  return
    car_consume_fuel(a_car,
		     fuel_consumed_with_a_move_in_an_element(a_car->speed.x,
							     a_car->speed.y,
							     speedup->x,
							     speedup->y,
							     current_tile)
		     );
}

/**
 * @brief Compute speed thanks to previous position.
 * @param a_car A car
 * @param previous_position The previous position of the car
 */
static inline
void
car_define_speed_with_previous_position_unsafe(car* a_car,
					       const car_position* previous_position)
{
  a_car->speed.x = a_car->position.x - previous_position->x;
  a_car->speed.y = a_car->position.y - previous_position->y;
}


#endif

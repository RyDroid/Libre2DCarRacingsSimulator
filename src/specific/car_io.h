/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions for input/output with a car.
 */


#ifndef CAR_IO_H
#define CAR_IO_H


#include "car.h"
#include <stdio.h>


static inline
void
car_position_to_stream_on_one_line_unsafe(const car_position* position,
					  FILE* stream)
{
  fprintf(stream, "(x=%5u, y=%5u)", position->x, position->y);
}

static inline
void
car_position_to_stdout_on_one_line_unsafe(const car_position* position)
{
  car_position_to_stream_on_one_line_unsafe(position, stdout);
}

static inline
void
car_speed_to_stream_on_one_line_unsafe(const car_speed* speed,
				       FILE* stream)
{
  fprintf(stream, "(x=%2hhd, y=%2hhd)", speed->x, speed->y);
}

static inline
void
car_speedup_to_stream_on_one_line_unsafe(const car_speedup* speedup,
					 FILE* stream)
{
  fprintf(stream, "(x=%2hhd, y=%2hhd)", speedup->x, speedup->y);
}

static inline
void
car_speedup_validate_turn_to_stream_unsafe(const car_speedup* speedup,
					   FILE* stream)
{
  fprintf(stream, "%hhd %hhd\n", speedup->x, speedup->y);
}

static inline
void
car_to_stream_on_one_line_unsafe(const car* car_to_print,
                                 FILE* stream)
{
  fputs("position=", stream);
  car_position_to_stream_on_one_line_unsafe(&car_to_print->position, stream);
  fputs(", speed=", stream);
  car_speed_to_stream_on_one_line_unsafe(&car_to_print->speed, stream);
  fprintf(stream,
          ", fuel=%5u, nbBoosts=%4u",
          car_to_print->fuel,
          car_to_print->nb_boosts);
}


#endif

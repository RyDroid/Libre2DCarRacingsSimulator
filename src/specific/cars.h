/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Structures and functions to manages cars.
 */


#ifndef CARS_H
#define CARS_H


/**
 * @brief Default number of cars
 */
#define DEFAULT_NB_CARS 3


#include "car.h"
#include "table_of_generic_static.h"
#include "queue_singly_linked_generic_static.h"


DEFINE_TABLE_TINY_OF(car)
typedef table_tiny_of_car table_of_cars;

static inline
void
table_of_cars_destruct_unsafe(table_of_cars* tab)
{
  TABLE_OF_GENERIC_STATIC_POINTER_DESTRUCT_UNSAFE(tab);
}

static inline
void
table_of_cars_destruct(table_of_cars* tab)
{
  if(tab != NULL)
    {
      table_of_cars_destruct_unsafe(tab);
    }
}

/**
 * @brief Creates an empty table of cars.
 * @return An empty table of cars
 */
static inline
table_of_cars
table_of_cars_create_empty()
{
  table_of_cars tab = { NULL, 0 };
  return tab;
}

/**
 * @brief Creates a table of cars with default values, except quantity of fuel.
 * @param fuel Quantity of fuel for each car
 * @return A table of cars with default values
 */
static inline
table_of_cars
table_of_cars_create_default_with_specified_fuel(unsigned int fuel)
{
  table_of_cars tab;
  tab.size = DEFAULT_NB_CARS;
  tab.elements = (car*) malloc(tab.size * sizeof(car));
  for(unsigned char i=0; i < tab.size; ++i)
    {
      tab.elements[i].position.x = 0;
      tab.elements[i].position.y = 0;
      tab.elements[i].fuel = fuel;
      tab.elements[i].nb_boosts = NB_BOOSTS_DEFAULT;
    }
  return tab;
}

/**
 * @brief Creates a table of cars with default values.
 * @return A table of cars with default values
 */
static inline
table_of_cars
table_of_cars_create_default()
{
  return table_of_cars_create_default_with_specified_fuel(0);
}


DEFINE_QUEUE_SINGLY_LINKED_GENERIC_STATIC(car)
typedef queue_singly_linked_node_of_car queue_node_of_car;
typedef queue_singly_linked_of_car queue_of_car;

static inline
queue_of_car
queue_of_car_create()
{
  queue_of_car queue = {NULL, NULL};
  return queue;
}

static inline
void
queue_of_car_destruct(queue_of_car* queue)
{
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_DESTRUCT(queue, car);
}

static inline
void
queue_of_car_push_value_unsafe(queue_of_car* queue,
			       const car* pos)
{
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_UNSAFE(queue,
							 car,
							 pos);
}

static inline
unsigned int
queue_of_car_size(const queue_of_car* queue)
{
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_SIZE_CODE(queue,
					       car);
}

static inline
car*
queue_of_car_to_table_with_size_unsafe(const queue_of_car* queue,
				       unsigned int size)
{
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_TO_TABLE_WITH_SIZE_CODE(queue,
							     size,
							     car);
}

static inline
table_of_cars
queue_of_car_to_table(const queue_of_car* queue)
{
  table_of_cars tab;
  tab.size = queue_of_car_size(queue);
  tab.elements = queue_of_car_to_table_with_size_unsafe(queue,
							tab.size);
  return tab;
}


#endif

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions for input/output with cars.
 */


#ifndef CARS_IO_H
#define CARS_IO_H


#include "cars.h"
#include "car_io.h"


/**
 * @brief Print cars to a stream as an ordered list.
 * @param cars A not null pointer on a table of cars
 * @param stream A stream to print in
 */
static inline
void
table_of_cars_to_stream_as_an_ordered_list_unsafe(const table_of_cars* cars,
                                                  FILE* stream)
{
  fputs(" 1. ", stream);
  car_to_stream_on_one_line_unsafe(&cars->elements[0], stream);
  for(unsigned char i=1; i < cars->size; ++i)
    {
      putc('\n', stream);
      fprintf(stream, "%2hhu. ", (unsigned char) (i+1));
      car_to_stream_on_one_line_unsafe(&cars->elements[i], stream);
    }
}

/**
 * @brief Print cars to a stream as an ordered list.
 * @param cars A pointer on a table of cars
 * @param stream A stream to print in
 * @return True of it succeeds, otherwise false
 */
static inline
bool
table_of_cars_to_stream_as_an_ordered_list(const table_of_cars* cars,
                                           FILE* stream)
{
  if(cars == NULL || cars->elements == NULL || stream == NULL)
    {
      return false;
    }
  table_of_cars_to_stream_as_an_ordered_list_unsafe(cars, stream);
  return true;
}

/**
 * @brief Print cars to a stdout as an ordered list.
 * @param cars A not null pointer on a table of cars
 */
static inline
void
table_of_cars_to_stdout_as_an_ordered_list_unsafe(const table_of_cars* cars)
{
  table_of_cars_to_stream_as_an_ordered_list_unsafe(cars, stdout);
}

/**
 * @brief Print cars to a stdout as an ordered list.
 * @param cars A pointer on a table of cars
 * @return True of it succeeds, otherwise false
 */
static inline
bool
table_of_cars_to_stdout_as_an_ordered_list(const table_of_cars* cars)
{
  if(cars == NULL || cars->elements == NULL)
    {
      return false;
    }
  table_of_cars_to_stream_as_an_ordered_list_unsafe(cars, stdout);
  return true;
}


#endif

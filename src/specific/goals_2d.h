/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <rydroid_dev@yahoo.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef GOALS_2D_H
#define GOALS_2D_H


#include "goal_2d.h"
#include "positions_2d_uint.h"
#include "tab_2d_char_plus.h"
#include "map_2d_tile.h"


typedef table_of_position_2d_uint table_of_goal_2d;
typedef table_of_goal_2d table_of_goal;

typedef queue_node_of_position_2d_uint queue_node_of_goal_2d;
typedef queue_node_of_goal_2d queue_node_of_goal;
typedef queue_of_position_2d_uint queue_of_goal_2d;
typedef queue_of_goal_2d queue_of_goal;


static inline
unsigned int
get_nb_goals_from_map_2d_unsafe(const map_2d_tile* map)
{
  return tab_2d_char_nb_occurrences_of_a_value(map, MAP_TILE_GOAL);
}

static inline
queue_of_goal_2d
get_queue_of_goals_from_map_2d_unsafe(const map_2d_tile* map)
{
  queue_of_goal_2d queue = queue_of_position_2d_uint_create();
  position_2d_uint pos;
  for(pos.y=0; pos.y < map->nb_lines; ++pos.y)
    {
      for(pos.x=0; pos.x < map->nb_columns; ++pos.x)
	{
	  if(map_2d_tile_get_element_value_unsafe(map, pos.y, pos.x)
	     == MAP_TILE_GOAL)
	    {
	      queue_of_position_2d_uint_push_value_unsafe(&queue, &pos);
	    }
	}
    }
  return queue;
}

static inline
table_of_goal_2d
get_table_of_goals_from_map_2d_unsafe(const map_2d_tile* map)
{
  table_of_goal_2d goals;
  goals.size = get_nb_goals_from_map_2d_unsafe(map);
  goals.elements = (goal_2d*) malloc(goals.size * sizeof(goal));
  for(unsigned int line=0, column, i=0; line < map->nb_lines; ++line)
    {
      for(column=0; column < map->nb_columns; ++column)
	{
	  if(map_2d_tile_get_element_value_unsafe(map, line, column)
	     == MAP_TILE_GOAL)
	    {
	      goals.elements[i].x = column;
	      goals.elements[i].y = line;
	      ++i;
	    }
	}
    }
  return goals;
}


#endif

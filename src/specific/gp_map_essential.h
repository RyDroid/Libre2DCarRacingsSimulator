/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef GP_MAP_ESSENTIAL_H
#define GP_MAP_ESSENTIAL_H


#include "map_2d_tile.h"
#include "cars.h"
#include <stdio.h>


/**
 * @brief Map and cars of a racing
 */
struct gp_map
{
  map_2d_char map;
  table_of_cars cars;
};

typedef struct gp_map gp_map;


/**
 * @brief Creates and returns an empty map with no car.
 * @return An empty map with no car
 */
static inline
gp_map
gp_map_create_empty()
{
  gp_map map;
  map.map = tab_2d_char_create_empty();
  map.cars = table_of_cars_create_empty();
  return map;
}

/**
 * @brief Destructs properly a not null pointer on pap and cars of a racing
 * @param map A not null pointer on map and cars of a racing
 */
static inline
void
gp_map_destruct_unsafe(gp_map* map)
{
  tab_2d_char_destruct(&map->map);
  table_of_cars_destruct_unsafe(&map->cars);
}


#endif

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions for input/output with map and cars of a racing.
 */


#ifndef GP_MAP_IO_H
#define GP_MAP_IO_H


#include "gp_map_essential.h"
#include "tab_2d_char_io.h"
#include "map_2d_tile_plus.h"
#include "cars_io.h"
#include <limits.h>


static inline
int
gp_map_print_size_to_plain_text_stream_unsafe(FILE* stream,
					      unsigned int nb_lines,
					      unsigned int nb_columns)
{
  return fprintf(stream, "%u %u",
		 nb_columns, nb_lines);
}

static inline
bool
gp_map_get_size_from_plain_text_stream_unsafe(FILE* stream,
					      unsigned int* nb_lines,
					      unsigned int* nb_columns)
{
  return fscanf(stream,
		"%u %u",
		nb_columns,
		nb_lines) == 2;
}

static inline
bool
gp_map_load_from_plain_text_stream_unsafe(gp_map* map,
					  FILE* stream,
					  bool remove_starts)
{
  unsigned int fuel;
  int c;
  
  gp_map_destruct_unsafe(map);

  /* Some memory may be wasted. */
  map->cars.elements = (car*) malloc(10 * sizeof(car));
  if(map->cars.elements == NULL)
    {
      return false;
    }
  map->cars.size = 0;
  
  if(!gp_map_get_size_from_plain_text_stream_unsafe(stream,
						    &map->map.nb_lines,
						    &map->map.nb_columns))
    {
      fputs("There is less than 2 unsigned integers on the first line of the file!",
	    stderr);
      fputc('\n', stderr);
      return false;
    }
  
  c = fscanf(stream,
	     " %u\n",
	     &fuel);
  if(c != 1)
    {
      fputs("There is only 2 unsigned integers on the first line of the file.",
	    stderr);
      fputs(" Fuel quantity is missing!",
	    stderr);
      fputc('\n', stderr);
      return false;
    }
  else if(c == 2)
    {
      fuel = UINT_MAX;
    }
  
  tab_2d_char_alloc_unsafe(&map->map);
  if(map->map.tab == NULL)
    {
      return false;
    }
  
  char* element_pointer;
  for(unsigned int line=0, column; line < map->map.nb_lines; ++line)
    {
      for(column=0; column < map->map.nb_columns; ++column)
        {
          do
            {
              c = fgetc(stream);
            }
          while (c == '\n' || c == '\r');

          if(c == EOF)
            {
              if(feof(stream))
                {
                  fputs("End-Of-File reached", stderr);
                }
              else
                {
                  fputs("End-Of-File not reached", stderr);
                }
              fprintf(stderr, " (line=%5u, column=%5u)\n", line, column);
              return false;
            }

          element_pointer = tab_2d_char_get_element_pointer_unsafe(&map->map, line, column);
          if(c == MAP_TILE_START1)
            {
              map->cars.elements[0] = car_create(column,
                                                 line,
                                                 fuel,
                                                 NB_BOOSTS_DEFAULT);
              ++map->cars.size;
              *element_pointer = remove_starts ? MAP_TILE_NORMAL : c;
            }
          else if(c == MAP_TILE_START2)
            {
              map->cars.elements[1] = car_create(column,
                                                 line,
                                                 fuel,
                                                 NB_BOOSTS_DEFAULT);
              ++map->cars.size;
              *element_pointer = remove_starts ? MAP_TILE_NORMAL : c;
            }
          else if(c == MAP_TILE_START3)
            {
              map->cars.elements[2] = car_create(column,
                                                 line,
                                                 fuel,
                                                 NB_BOOSTS_DEFAULT);
              ++map->cars.size;
              *element_pointer = remove_starts ? MAP_TILE_NORMAL : c;
            }
          else
            {
              *element_pointer = c;
            }
        }
    }
  
  if(map->cars.size == 0)
    {
      table_of_cars_destruct_unsafe(&map->cars);
      map->cars = table_of_cars_create_default_with_specified_fuel(fuel);
    }

  return true;
}

static inline
bool
gp_map_load_from_stream_unsafe(gp_map* map,
                               FILE* stream,
                               bool remove_starts)
{
  return gp_map_load_from_plain_text_stream_unsafe(map,
						   stream,
						   remove_starts);
}

static inline
bool
gp_map_load_from_stream(gp_map* map,
                        FILE* stream,
                        bool remove_starts)
{
  return
      map == NULL || stream == NULL
      ? false
      : gp_map_load_from_stream_unsafe(map, stream, remove_starts);
}

static inline
bool
gp_map_load_from_file_path_unsafe(gp_map* map,
                                  const char* file_path,
                                  bool remove_starts)
{
  FILE* file = fopen(file_path, "r");
  if(file == NULL)
    {
      return false;
    }
  bool res = gp_map_load_from_stream_unsafe(map, file, remove_starts);
  fclose(file);
  return res;
}

static inline
bool
gp_map_load_from_file_path(gp_map* map,
                           const char* file_path,
                           bool remove_starts)
{
  return
      map == NULL || file_path == NULL || *file_path == '\0'
      ? false
      : gp_map_load_from_file_path_unsafe(map, file_path, remove_starts);
}

static inline
bool
gp_map_print_map_to_stream_without_grid(const gp_map* map,
                                        FILE* stream)
{
  if(map == NULL)
    {
      return false;
    }
  tab_2d_char_print_stream_without_grid(&map->map, stream);
  return true;
}

static inline
void
gp_map_print_map_to_stdout_without_grid(const gp_map* map)
{
  gp_map_print_map_to_stream_without_grid(map, stdout);
}

/**
 * @brief Print cars of a gp_map to a stream as an ordered list.
 * @param gp_map Map and cars for a racing
 * @param stream A stream to print in
 * @return True of it succeeds, otherwise false
 */
static inline
bool
gp_map_print_cars_to_stream_as_an_ordered_list(const gp_map* map,
                                               FILE* stream)
{
  if(map == NULL)
    {
      return false;
    }
  return table_of_cars_to_stream_as_an_ordered_list(&map->cars, stream);
}

/**
 * @brief Print cars of a gp_map to stdout as an ordered list.
 * @param gp_map Map and cars for a racing
 * @return True of it succeeds, otherwise false
 */
static inline
bool
gp_map_print_cars_to_stdout_as_an_ordered_list(const gp_map* map)
{
  return gp_map_print_cars_to_stream_as_an_ordered_list(map, stdout);
}

static inline
void
gp_map_print_to_plain_text_stream_unsafe(FILE* stream,
					 const map_2d_char* map,
					 bool print_fuel)
{
  gp_map_print_size_to_plain_text_stream_unsafe(stream,
						map->nb_lines,
						map->nb_columns);
  if(print_fuel)
    {
      
      fprintf(stream, " %u",
	      get_fuel_quantity_for_a_map_2d_unsafe(map));
    }
  fputc('\n', stream);
  
  tab_2d_char_print_stream_without_grid_unsafe(map, stream);
}


#endif

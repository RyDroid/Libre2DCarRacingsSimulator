/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions for input/output with map and cars of a racing.
 */


#ifndef GP_MAP_IO_HPP
#define GP_MAP_IO_HPP


extern "C" {
#include "gp_map_io.h"
}
#include "png_image_for_map_2d.hpp"


static inline
bool
gp_map_load_from_png_image_path_unsafe(gp_map* map,
				       const char* file_path,
				       FILE* error_stream = stderr)
{
  gp_map_destruct_unsafe(map);
  
  if(!png_image_path_to_map_2d_unsafe(file_path, &map->map, error_stream))
    {
      return false;
    }
  
  map->cars = map_2d_tile_get_table_of_cars_unsafe(&map->map,
						   get_fuel_quantity_for_a_map_2d_unsafe(&map->map));
  if(map->cars.elements == NULL)
    {
      return false;
    }
  
  return true;
}


#endif

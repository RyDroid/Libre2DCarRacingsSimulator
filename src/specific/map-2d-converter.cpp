/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


extern "C"
{
#include "gp_map_io.h"
#include "map_file_type.h"
#include "command_line_interface.h"
#include "io_functions.h"
}
#include "png_image_for_map_2d.hpp"


void
help_to_stream_unsafe(FILE* stream,
		       const char* program_command)
{
  fprintf(stream,
	  "Usage: %s [options] "
	  "file-path-to-a-track-png file-path-to-a-track-converted",
	  program_command);
  fputc('\n', stream);

  /* TODO print options that are managed */
}


int
main(int argc, const char* argv[])
{
  if(argc == 1)
    {
      help_to_stream_unsafe(stderr, argv[0]);
      return EXIT_FAILURE;
    }

  /* Arguments management with message and exit result */
  if (cstring_equals(argv[1], "?")        ||
      cstring_equals(argv[1], "help")     ||
      cstring_equals(argv[1], "manual")   ||
      cstring_equals(argv[1], "info")     ||
      cstring_equals(argv[1], "usage")    ||
      is_a_help_cstring_option_unsafe(argv[1]))
    {
      help_to_stream_unsafe(stdout, argv[0]);
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--license") ||
     cstring_equals(argv[1], "--licence") ||
     cstring_equals(argv[1], "--copyright"))
    {
      puts("This program is under GNU Affero General Public License 3 "
	   "as published by the Free Software Foundation, "
	   "either version 3 of the License, "
	   "or (at your option) any later version.");
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--author"))
    {
      puts("The author of this program is Nicola Spanti, "
	   "also known as RyDroid.");
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--auteur"))
    {
      puts("Le créateur de ce programme est Nicola Spanti, "
	   "aussi connu sous le pseudonyme RyDroid.");
      return EXIT_SUCCESS;
    }
  if(argc < 3)
    {
      help_to_stream_unsafe(stderr, argv[0]);
      return EXIT_FAILURE;
    }

  /* Arguments management for options */
  bool print_fuel = true;
  const char* input_file_path  = argv[argc -2];
  const char* output_file_path = argv[argc -1];
  map_file_format input_file_format  = MAP_FILE_TYPE_UNDEFINED;
  map_file_format output_file_format = MAP_FILE_TYPE_UNDEFINED;
  for(int i=1; i < argc -2; ++i)
    {
      if(cstring_equals(argv[i], "--fuel")      ||
	 cstring_equals(argv[i], "--fuel=true") ||
	 cstring_equals(argv[i], "--fuel=yes")  ||
	 cstring_equals(argv[i], "--carburant"))
	{
	  print_fuel = true;
	}
      else if(cstring_equals(argv[i], "--no-fuel")    ||
	      cstring_equals(argv[i], "--fuel=false") ||
	      cstring_equals(argv[i], "--fuel=no")    ||
	      cstring_equals(argv[i], "--pas-carburant"))
	{
	  print_fuel = false;
	}
      else if(cstring_equals(argv[i], "--stdin"))
	{
	  puts("--stdin is not managed yet.");
	}
      else if(cstring_equals(argv[i], "--stdout"))
	{
	  puts("--stdout is not managed yet.");
	}
      else if(cstring_equals(argv[i], "--input-type") ||
	      cstring_equals(argv[i], "--input-format"))
	{
	  puts("--input-type is not managed yet.");
	  ++i;
	  if(is_plain_text_type_cstring(argv[i]))
	    {
	      input_file_format = MAP_FILE_TYPE_PLAIN_TEXT;
	    }
	  else if(is_png_type_cstring(argv[i]))
	    {
	      input_file_format = MAP_FILE_TYPE_PNG;
	    }
	  else if(is_ppm_type_cstring(argv[i]))
	    {
	      input_file_format = MAP_FILE_TYPE_PPM;
	    }
	  else if(is_webp_type_cstring(argv[i]))
	    {
	      input_file_format = MAP_FILE_TYPE_WEBP;
	    }
	  else if(cstring_equals(argv[i], "jpg")  ||
		  cstring_equals(argv[i], "JPG")  ||
		  cstring_equals(argv[i], "jpeg") ||
		  cstring_equals(argv[i], "JPEG"))
	    {
	      fputs("JPEG is not managed "
		    "because it is a lossy compression format.",
		    stderr);
	      fputc('\n', stderr);
	      return EXIT_FAILURE;
	    }
	}
      else if(cstring_equals(argv[i], "--output-type") ||
	      cstring_equals(argv[i], "--output-format"))
	{
	  puts("--output-type is not managed yet.");
	  ++i;
	  if(is_plain_text_type_cstring(argv[i]))
	    {
	      output_file_format = MAP_FILE_TYPE_PLAIN_TEXT;
	    }
	  else if(is_png_type_cstring(argv[i]))
	    {
	      output_file_format = MAP_FILE_TYPE_PNG;
	    }
	  else if(is_ppm_type_cstring(argv[i]))
	    {
	      output_file_format = MAP_FILE_TYPE_PPM;
	    }
	  else if(is_webp_type_cstring(argv[i]))
	    {
	      output_file_format = MAP_FILE_TYPE_WEBP;
	    }
	  else if(is_jpeg_type_cstring(argv[i]))
	    {
	      fputs("JPEG is not managed "
		    "because it is a lossy compression format.",
		    stderr);
	      fputc('\n', stderr);
	      fputs("If you need a JPEG and a command line tool, "
		    "you can use convert of ImageMagick. "
		    "It is a free/libre and gratis program. "
		    "http://imagemagick.org/",
		    stderr);
	      fputc('\n', stderr);
	      return EXIT_FAILURE;
	    }
	}
      else if(cstring_equals(argv[i], "--png2txt") ||
	      cstring_equals(argv[i], "--png2text"))
	{
	  input_file_format  = MAP_FILE_TYPE_PNG;
	  output_file_format = MAP_FILE_TYPE_PLAIN_TEXT;
	}
      else if(cstring_equals(argv[i], "--ppm2txt") ||
	      cstring_equals(argv[i], "--ppm2text"))
	{
	  input_file_format  = MAP_FILE_TYPE_PPM;
	  output_file_format = MAP_FILE_TYPE_PLAIN_TEXT;
	}
      else if(cstring_equals(argv[i], "--txt2png") ||
	      cstring_equals(argv[i], "--text2png"))
	{
	  input_file_format  = MAP_FILE_TYPE_PLAIN_TEXT;
	  output_file_format = MAP_FILE_TYPE_PNG;
	}
      else
	{
	  fputs("Option ", stderr);
	  fputs(argv[i], stderr);
	  fputs(" does not exist.", stderr);
	  fputc('\n', stderr);
	}
    }
  
  if(input_file_format == MAP_FILE_TYPE_UNDEFINED)
    {
      if(is_plain_text_file_path(input_file_path))
	{
	  input_file_format = MAP_FILE_TYPE_PLAIN_TEXT;
	  fputs("Plain text is not yet managed as an input.", stderr);
	  fputc('\n', stderr);
	  return EXIT_FAILURE;
	}
      else if(is_png_file_path(input_file_path))
	{
	  input_file_format = MAP_FILE_TYPE_PNG;
	}
      else if(is_ppm_file_path(input_file_path))
	{
	  input_file_format = MAP_FILE_TYPE_PPM;
	  fputs("PPM is not yet managed as an input.", stderr);
	  fputc('\n', stderr);
	  return EXIT_FAILURE;
	}
      else if(is_webp_file_path(input_file_path))
	{
	  input_file_format = MAP_FILE_TYPE_WEBP;
	  fputs("WebP is not managed as an input.", stderr);
	  fputc('\n', stderr);
	  return EXIT_FAILURE;
	}
      else
	{
	  input_file_format = MAP_FILE_TYPE_PNG;
	  puts("Input file format is undefined. "
	       "It is now assumed to be PNG.");
	}
    }
  
  if(output_file_format == MAP_FILE_TYPE_UNDEFINED)
    {
      if(is_plain_text_file_path(output_file_path))
	{
	  output_file_format = MAP_FILE_TYPE_PLAIN_TEXT;
	}
      else if(is_png_file_path(output_file_path))
	{
	  output_file_format = MAP_FILE_TYPE_PNG;
	  fputs("PNG is not yet managed as an output.", stderr);
	  fputc('\n', stderr);
	  return EXIT_FAILURE;
	}
      else if(is_ppm_file_path(output_file_path))
	{
	  output_file_format = MAP_FILE_TYPE_PPM;
	  fputs("PPM is not yet managed as an output.", stderr);
	  fputc('\n', stderr);
	  return EXIT_FAILURE;
	}
      else if(is_webp_file_path(output_file_path))
	{
	  output_file_format = MAP_FILE_TYPE_WEBP;
	  fputs("WebP is not managed as an output.", stderr);
	  fputc('\n', stderr);
	  return EXIT_FAILURE;
	}
      else
	{
	  output_file_format = MAP_FILE_TYPE_PLAIN_TEXT;
	  puts("Output file format is undefined. "
	       "It is now assumed to be plain text.");
	}
    }

  if(input_file_format == output_file_format)
    {
      fputs("The input file format is the same as the output file format!",
	    stderr);
      return EXIT_FAILURE;
    }
  
  map_2d_char map;
  if(!png_image_path_to_map_2d_unsafe(input_file_path, &map, stderr))
    {
      return EXIT_FAILURE;
    }
  
  FILE* generated_file =
    fopen_with_error_message_to_stderr_unsafe(output_file_path, "w");
  if(generated_file == NULL)
    {
      return EXIT_FAILURE;
    }
  if(output_file_format == MAP_FILE_TYPE_PLAIN_TEXT)
    {
      gp_map_print_to_plain_text_stream_unsafe(generated_file,
					       &map, print_fuel);
    }
  else
    {
      fputs("This output file format is not managed!", stderr);
      fputc('\n', stderr);
      fclose(generated_file);
      tab_2d_char_free_unsafe(&map);
      return EXIT_FAILURE;
    }
  fclose(generated_file);
  
  tab_2d_char_free_unsafe(&map);
  
  return EXIT_SUCCESS;
}

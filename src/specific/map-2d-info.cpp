/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


extern "C"
{
#include "goals_2d.h"
#include "command_line_interface.h"
#include "number_functions.h"
#include "math_macros.h"
#include "io_functions.h"
}
#include "gp_map_io.hpp"


void
help_to_stream_unsafe(FILE* stream,
		       const char* program_command)
{
  fprintf(stream,
	  "Usage: %s action [options] file-path-to-a-track",
	  program_command);
  fputc('\n', stream);
  fputc('\n', stream);

  fprintf(stream,
	  "%s is a simple command line tool to have information about a 2D map.",
	  program_command);
  fputc('\n', stream);
  fputc('\n', stream);

  fputs("Actions:", stream);
  fputc('\n', stream);
  one_line_action_helper_to_stream_unsafe("help",
					  "This help message",
					  stream);
  one_line_action_helper_to_stream_unsafe("load",
					  "Load a 2D map",
					  stream);
  one_line_action_helper_to_stream_unsafe("print",
					  "Print a 2D map",
					  stream);
  one_line_action_helper_to_stream_unsafe("nb-lines",
					  "Print the number of lines of a 2D map",
					  stream);
  one_line_action_helper_to_stream_unsafe("nb-columns",
					  "Print the number of columns of a 2D map",
					  stream);
  one_line_action_helper_to_stream_unsafe("dimension",
					  "Print the dimension of a 2D map",
					  stream);
  one_line_action_helper_to_stream_unsafe("nb-goals",
					  "Print the number of goals of a 2D map",
					  stream);
  one_line_action_helper_to_stream_unsafe("goals",
					  "Print the goals of a 2D map",
					  stream);
  one_line_action_helper_to_stream_unsafe("starts",
					  "Print the potential starts of a 2D map",
					  stream);
  one_line_action_helper_to_stream_unsafe("cars",
					  "Print the potential cars of a 2D map",
					  stream);
  one_line_action_helper_to_stream_unsafe("fuel",
					  "Print the fuel quantity of a 2D map",
					  stream);
}

bool
remove_starts_from_map_unsafe(unsigned int argc,
			      const char* argv[])
{
  bool answer = false;
  for(unsigned int i=0; i < argc; ++i)
    {
      if(cstring_equals(argv[i], "--remove-starts") ||
	 cstring_equals(argv[i], "--delete-starts"))
	{
	  answer = true;
	}
      else if(cstring_equals(argv[i], "--keep-starts"))
	{
	  answer = false;
	}
    }
  return answer;
}

bool
load_map_2d_from_plain_text(gp_map* map,
			    int argc, const char* argv[])
{
  FILE* file = fopen_with_error_message_to_stderr_unsafe(argv[argc-1], "r");
  if(file == NULL)
    {
      return false;
    }
  if(!gp_map_load_from_stream(map, file,
			      remove_starts_from_map_unsafe(argc -3 /* program + action + track_path  */,
							    &argv[2])))
    {
      fclose(file);
      return false;
    }
  fclose(file);
  return true;
}

bool
load_map_2d_from_png(gp_map* map,
		     int argc, const char* argv[])
{
  if(!gp_map_load_from_png_image_path_unsafe(map, argv[argc -1], stderr))
    {
      return false;
    }
  return true;
}

bool
load_map_2d(gp_map* map,
	    int argc, const char* argv[])
{
  if(cstring_ends_with(argv[argc-1], ".png") ||
     cstring_ends_with(argv[argc-1], ".PNG"))
    {
      return load_map_2d_from_png(map, argc, argv);
    }
  return load_map_2d_from_plain_text(map, argc, argv);
}

#define load_map_2d_main(map_ptr)		\
  if(!load_map_2d(map_ptr, argc, argv))		\
    {						\
      return EXIT_FAILURE;			\
    }


int
main(int argc, const char* argv[])
{
  if(argc == 1)
    {
      help_to_stream_unsafe(stderr, argv[0]);
      return EXIT_FAILURE;
    }

  /* Argument management with message and exit result */
  if (cstring_equals(argv[1], "?")        ||
      cstring_equals(argv[1], "help")     ||
      cstring_equals(argv[1], "manual")   ||
      cstring_equals(argv[1], "info")     ||
      cstring_equals(argv[1], "usage")    ||
      is_a_help_cstring_option_unsafe(argv[1]))
    {
      help_to_stream_unsafe(stdout, argv[0]);
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--license") ||
     cstring_equals(argv[1], "--licence") ||
     cstring_equals(argv[1], "--copyright"))
    {
      puts("This program is under GNU Affero General Public License 3 "
	   "as published by the Free Software Foundation, "
	   "either version 3 of the License, "
	   "or (at your option) any later version.");
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--author"))
    {
      puts("The author of this program is Nicola Spanti, "
	   "also known as RyDroid.");
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--auteur"))
    {
      puts("Le créateur de ce programme est Nicola Spanti, "
	   "aussi connu sous le pseudonyme RyDroid.");
      return EXIT_SUCCESS;
    }
  if(argc < 3)
    {
      help_to_stream_unsafe(stderr, argv[0]);
      return EXIT_FAILURE;
    }

  /* Actions management */
  if(cstring_equals(argv[1], "load"))
    {
      /* debugging purpose */
      
      gp_map map = gp_map_create_empty();
      load_map_2d_main(&map);
      
      if(!(argc > 3 &&
	   cstring_equals(argv[2], "--just-exit-code")))
	{
	  puts("done");
	}
      
      gp_map_destruct_unsafe(&map);
    }
  else if(cstring_equals(argv[1], "print"))
    {
      gp_map map = gp_map_create_empty();
      load_map_2d_main(&map);
      
      gp_map_print_map_to_stdout_without_grid(&map);
      
      gp_map_destruct_unsafe(&map);
    }
  else if(cstring_equals(argv[1], "nb-lines") ||
	  cstring_equals(argv[1], "nb_lines") ||
	  cstring_equals(argv[1], "nbLines")  ||
	  cstring_equals(argv[1], "height"))
    {
      FILE* file = fopen_with_error_message_to_stderr_unsafe(argv[argc-1], "r");
      if(file == NULL)
	{
	  return EXIT_FAILURE;
	}
      
      unsigned int nb_lines;
      unsigned int nb_columns;
      gp_map_get_size_from_plain_text_stream_unsafe(file,
						    &nb_lines,
						    &nb_columns);
      fclose(file);
      
      printf("%u", nb_lines);
      putchar('\n');
    }
  else if(cstring_equals(argv[1], "nb-columns") ||
	  cstring_equals(argv[1], "nb_columns") ||
	  cstring_equals(argv[1], "nbColumns")  ||
	  cstring_equals(argv[1], "width"))
    {
      FILE* file = fopen_with_error_message_to_stderr_unsafe(argv[argc-1], "r");
      if(file == NULL)
	{
	  return EXIT_FAILURE;
	}
      
      unsigned int nb_lines;
      unsigned int nb_columns;
      gp_map_get_size_from_plain_text_stream_unsafe(file,
						    &nb_lines,
						    &nb_columns);
      fclose(file);
      
      printf("%u", nb_columns);
      putchar('\n');
    }
  else if(cstring_equals(argv[1], "dim")       ||
	  cstring_equals(argv[1], "dimension") ||
	  cstring_equals(argv[1], "size"))
    {
      FILE* file = fopen_with_error_message_to_stderr_unsafe(argv[argc-1], "r");
      if(file == NULL)
	{
	  return EXIT_FAILURE;
	}
      
      unsigned int nb_lines;
      unsigned int nb_columns;
      gp_map_get_size_from_plain_text_stream_unsafe(file,
						    &nb_lines,
						    &nb_columns);
      fclose(file);
      
      /* Creating a format in order that numbers are right-aligned */
      const unsigned char nb_max_digits =
	MAX(number_of_digits_of_unsigned_int(nb_lines),
	    number_of_digits_of_unsigned_int(nb_columns));
      const unsigned int nb_format_length =
	number_of_digits_of_unsigned_int(nb_max_digits) + 3;
      char* nb_format = (char*) malloc(nb_format_length);
      sprintf(nb_format, " %u ", nb_max_digits);
      *nb_format = '%';
      *(nb_format + nb_format_length -2) = 'u';
      
      fputs("Number of lines:   ", stdout);
      printf(nb_format, nb_lines);
      putchar('\n');
      
      fputs("Number of columns: ", stdout);
      printf(nb_format, nb_columns);
      putchar('\n');
      
      free(nb_format);
    }
  else if(cstring_equals(argv[1], "nb-goals") ||
	  (argc > 3 &&
	   cstring_equals(argv[1], "goals") &&
	   cstring_equals(argv[2], "--nb")))
    {
      gp_map map = gp_map_create_empty();
      load_map_2d_main(&map);
      
      printf("%u", get_nb_goals_from_map_2d_unsafe(&map.map));
      putchar('\n');
      
      gp_map_destruct_unsafe(&map);
    }
  else if(cstring_equals(argv[1], "goals"))
    {
      gp_map map = gp_map_create_empty();
      load_map_2d_main(&map);
      
      const char* format;
      if(argc > 3 && cstring_equals(argv[2], "--format-csv"))
	{
	  puts("line,column");
	  format = "%u,%u";
	}
      else if(argc > 3 && cstring_equals(argv[2], "--format-y-x"))
	{
	  format = "* y=%u, x=%u";
	}
      else if(argc > 3 &&
	      cstring_starts_with(argv[2], "--format-") &&
	      !cstring_equals(argv[2] +9, "line-column"))
	{
	  fputs("Unknow format", stderr);
	  fputc('\n', stderr);
	  gp_map_destruct_unsafe(&map);
	  return EXIT_FAILURE;
	}
      else
	{
	  format = "* line=%u, column=%u";
	}
      
      table_of_goal goals = get_table_of_goals_from_map_2d_unsafe(&map.map);
      for(unsigned int i=0; i < goals.size; ++i)
	{
	  printf(format,
		 goals.elements[i].y,
		 goals.elements[i].x);
	  putchar('\n');
	}
      TABLE_OF_GENERIC_STATIC_POINTER_FREE_UNSAFE(&goals);
      
      gp_map_destruct_unsafe(&map);
    }
  else if(cstring_equals(argv[1], "starts"))
    {
      gp_map map = gp_map_create_empty();
      load_map_2d_main(&map);

      if(map.cars.size == 0)
	{
	  puts("There is no start!");
	}
      else
	{
	  fputs(" 1. ", stdout);
	  car_position_to_stdout_on_one_line_unsafe(&map.cars.elements[0].position);
	  for(unsigned char i=1; i < map.cars.size; ++i)
	    {
	      putchar('\n');
	      printf("%2hhu. ", (unsigned char) (i+1));
	      car_position_to_stdout_on_one_line_unsafe(&map.cars.elements[i].position);
	    }
	  putchar('\n');
	}
      
      gp_map_destruct_unsafe(&map);
    }
  else if(cstring_equals(argv[1], "cars"))
    {
      gp_map map = gp_map_create_empty();
      load_map_2d_main(&map);
      
      gp_map_print_cars_to_stdout_as_an_ordered_list(&map);
      putchar('\n');
      
      gp_map_destruct_unsafe(&map);
    }
  else if(cstring_equals(argv[1], "fuel"))
    {
      gp_map map = gp_map_create_empty();
      load_map_2d_main(&map);
      
      if(map.cars.size == 0 ||
	 map.cars.elements == NULL)
	{
	  puts("No information was found about fuel quantity");
	}
      else
	{
	  printf("%u", map.cars.elements[0].fuel);
	  putchar('\n');
	}
      
      gp_map_destruct_unsafe(&map);
    }
  else
    {
      fprintf(stderr, "Action %s is not known", argv[1]);
      fputc('\n', stderr);
      return EXIT_FAILURE;
    }
  
  return EXIT_SUCCESS;
}

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief 2D map composed of tiles.
 */


#ifndef MAP_2D_TILE_H
#define MAP_2D_TILE_H


#include "tab_2d_char_essential.h"
#include "map_tile.h"


typedef tab_2d_char map_2d_char;
typedef map_2d_char map_2d_tile;


/**
 * @brief Returns the value of an element of a 2D map composed of tiles.
 * @param tab_2d A pointer of a 2D map composed of tiles
 * @param line A line of the 2D map composed of tiles
 * @param column A column of the 2D map composed of tiles
 * @return The value of the element (line, column) of the 2D map composed of tiles
 */
static inline
map_tile
map_2d_tile_get_element_value_unsafe(const map_2d_tile* map,
                                     unsigned int line,
                                     unsigned int column)
{
  return
      tab_2d_char_element_exists(map, line, column)
      ? (map_tile) tab_2d_char_get_element_value_unsafe(map, line, column)
      : MAP_TILE_OUT;
}

/**
 * @brief Checks if an element/position of a 2D map composed of tiles is possible for a car.
 * @param tab_2d A pointer of a 2D map composed of tiles
 * @param line A line of the 2D map composed of tiles
 * @param column A column of the 2D map composed of tiles
 * @return True if an element/position of a 2D map composed of tiles is possible for a car, otherwise false
 */
static inline
bool
map_2d_tile_is_element_possible_for_a_car(const map_2d_tile* map,
                                          unsigned int line,
                                          unsigned int column)
{
  return
      map_tile_is_possible_for_a_car(
        map_2d_tile_get_element_value_unsafe(map, line, column)
        );
}


#endif

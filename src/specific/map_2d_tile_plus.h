/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef MAP_2D_TILE_PLUS_H
#define MAP_2D_TILE_PLUS_H


#include "map_2d_tile.h"
#include "cars.h"


static inline
unsigned int
get_fuel_quantity_for_a_map_2d_unsafe(const map_2d_char* map)
{
  /* Algorithm to find a good fuel quantity based on... chance!
     TODO do something better */
  return (unsigned int) (map->nb_lines * map->nb_columns * 0.08);
}

static inline
unsigned int
get_fuel_quantity_for_a_map_2d(const map_2d_char* map)
{
  if(tab_2d_char_is_init(map))
    {
      return get_fuel_quantity_for_a_map_2d_unsafe(map);
    }
  return 0;
}

static inline
table_of_cars
map_2d_tile_get_table_of_cars_unsafe(const map_2d_tile* map,
				     unsigned int fuel_for_each_car)
{
  table_of_cars cars;
  
  /* Some memory may be wasted. */
  cars.size = 0;
  cars.elements = (car*) malloc(10 * sizeof(car));
  if(cars.elements == NULL)
    {
      return cars;
    }
  
  map_tile tile;
  for(unsigned int line=0, column; line < map->nb_lines; ++line)
    {
      for(column=0; column < map->nb_columns; ++column)
	{
	  tile = map_2d_tile_get_element_value_unsafe(map, line, column);
	  
	  if(tile == MAP_TILE_START1)
	    {
	      cars.elements[0] = car_create(column,
					    line,
					    fuel_for_each_car,
					    NB_BOOSTS_DEFAULT);
              ++cars.size;
	    }
	  else if(tile == MAP_TILE_START2)
	    {
	      cars.elements[1] = car_create(column,
					    line,
					    fuel_for_each_car,
					    NB_BOOSTS_DEFAULT);
              ++cars.size;
	    }
	  else if(tile == MAP_TILE_START3)
	    {
	      cars.elements[2] = car_create(column,
					    line,
					    fuel_for_each_car,
					    NB_BOOSTS_DEFAULT);
              ++cars.size;
	    }
	}
    }
  
  return cars;
}


#endif

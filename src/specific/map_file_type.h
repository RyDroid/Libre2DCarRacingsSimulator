/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef MAP_FILE_TYPE_H
#define MAP_FILE_TYPE_H


#include "file_type.h"


enum map_file_format
  {
    MAP_FILE_TYPE_UNDEFINED  = FILE_TYPE_UNDEFINED,
    MAP_FILE_TYPE_PLAIN_TEXT = FILE_TYPE_PLAIN_TEXT,
    MAP_FILE_TYPE_PNG        = FILE_TYPE_PNG,
    MAP_FILE_TYPE_PPM        = FILE_TYPE_PPM,
    MAP_FILE_TYPE_WEBP       = FILE_TYPE_WEBP
  };
typedef enum map_file_format map_file_format;


static inline
map_file_format
get_map_file_format_with_file_path(const char* file_path)
{
  if(is_plain_text_file_path(file_path))
    {
      return MAP_FILE_TYPE_PLAIN_TEXT;
    }
  if(is_png_file_path(file_path))
    {
      return MAP_FILE_TYPE_PNG;
    }
  if(is_ppm_file_path(file_path))
    {
      return MAP_FILE_TYPE_PPM;
    }
  if(is_webp_file_path(file_path))
    {
      return MAP_FILE_TYPE_WEBP;
    }
  return MAP_FILE_TYPE_UNDEFINED;
}


#endif

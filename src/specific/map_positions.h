/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef MAP_POSITIONS_H
#define MAP_POSITIONS_H


#include "map_2d_position.h"
#include "table_of_generic_static.h"


DEFINE_TABLE_OF(map_position)


/**
 * @brief Destructs properly a not null pointer on a table of map positions.
 * @param tab A not null pointer on a table of map positions
 */
static inline
void
table_of_map_position_destruct_unsafe(table_of_map_position* tab)
{
  TABLE_OF_GENERIC_STATIC_POINTER_FREE_UNSAFE(tab);
}

/**
 * @brief Destructs properly a pointer on a table of map positions.
 * @param tab A pointer on a table of map positions
 */
static inline
void
table_of_map_position_destruct(table_of_map_position* tab)
{
  if(tab != NULL)
    {
      table_of_map_position_destruct_unsafe(tab);
    }
}

/**
 * @brief Creates and returns an empty table of map positions.
 * @return An empty table of map positions
 */
static inline
table_of_map_position
table_of_map_position_create_empty()
{
  table_of_map_position tab = { NULL, 0 };
  return tab;
}

static inline
map_position*
table_of_map_position_get_closest_position_unsafe(const table_of_map_position* tab,
						  const map_position* position)
{
  map_position* closest = &tab->elements[0];
  double shortest_distance = distance_between_2_positions_2d_uint(*closest, *position);
  double tmp_distance;
  for(unsigned int i=1; i < tab->size; ++i)
    {
      tmp_distance = distance_between_2_positions_2d_uint(tab->elements[i],
							  *position);
      if(tmp_distance < shortest_distance)
	{
	  closest = &tab->elements[i];
	}
    }
  return closest;
}

static inline
map_position*
table_of_map_position_get_closest_position(const table_of_map_position* tab,
					   const map_position* position)
{
  if(tab == NULL           ||
     tab->elements == NULL ||
     position == NULL)
    {
      return NULL;
    }
  return table_of_map_position_get_closest_position_unsafe(tab, position);
}


#endif

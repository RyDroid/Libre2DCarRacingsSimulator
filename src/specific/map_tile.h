/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Structure and functions to manage a tile of a racing map.
 */


#ifndef MAP_TILE_H
#define MAP_TILE_H


#include "bool.h"


/**
 * @brief A tile of a racing map
 */
enum map_tile
  {
    MAP_TILE_OUT    = '.',
    MAP_TILE_NORMAL = '#',
    MAP_TILE_SAND   = '~',
    MAP_TILE_GOAL   = '=',
    MAP_TILE_START0 = '0',
    MAP_TILE_START1 = '1',
    MAP_TILE_START2 = '2',
    MAP_TILE_START3 = '3',
    MAP_TILE_START4 = '4',
    MAP_TILE_START5 = '5',
    MAP_TILE_START6 = '6',
    MAP_TILE_START7 = '7',
    MAP_TILE_START8 = '8',
    MAP_TILE_START9 = '9'
  };

typedef enum map_tile map_tile;


/**
 * @brief Checks if a tile value reduces speed of a car.
 * @param tile Tile value to check
 * @return True if the tile value reduces speed of a car, otherwise false
 */
static inline
bool
map_tile_is_reducing_speed(char tile)
{
  return
    tile == MAP_TILE_OUT ||
    tile == MAP_TILE_SAND;  
}

/**
 * @brief Checks if a tile value is a start.
 * @param tile Tile value to check
 * @return True if the tile value is a start, otherwise false
 */
static inline
bool
map_tile_is_a_start(char tile)
{
  return
    tile == MAP_TILE_START0 ||
    tile == MAP_TILE_START1 ||
    tile == MAP_TILE_START2 ||
    tile == MAP_TILE_START3 ||
    tile == MAP_TILE_START4 ||
    tile == MAP_TILE_START5 ||
    tile == MAP_TILE_START6 ||
    tile == MAP_TILE_START7 ||
    tile == MAP_TILE_START8 ||
    tile == MAP_TILE_START9;
}

/**
 * @brief Checks if a tile value is similar to normal.
 * @param tile Tile value to check
 * @return True if the tile value is similar to normal, otherwise false
 */
static inline
bool
map_tile_is_similar_to_normal(char tile)
{
  return
      tile == MAP_TILE_NORMAL ||
      tile == MAP_TILE_GOAL   ||
      map_tile_is_a_start(tile);
}

/**
 * @brief Checks if a tile value is possible of a car.
 * @param tile Tile value to check
 * @return True if the tile value is possible for a car, otherwise false
 */
static inline
bool
map_tile_is_possible_for_a_car(char tile)
{
  return tile != MAP_TILE_OUT;
}

/**
 * @brief Checks if an argument of type char is a tile of map.
 * @param value Value to check
 * @return True if the value is a tile of map, otherwise false
 */
static inline
bool
char_is_map_tile(char value)
{
  return
      map_tile_is_similar_to_normal(value) ||
      map_tile_is_reducing_speed(value);
}


#endif

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef PNG_IMAGE_FOR_MAP_2D_HPP
#define PNG_IMAGE_FOR_MAP_2D_HPP


#include "png_image.hpp"
#include "tab_2d_char_essential.h"


bool
color_channel_fix_approximation_unsafe(unsigned char* color_channel)
{
  if(*color_channel != 0 && *color_channel < 55)
    {
      *color_channel = 0;
      return true;
    }
  if(*color_channel != 255 && *color_channel > 200)
    {
      *color_channel = 255;
      return true;
    }
  return false;
}

bool
png_pixel_fix_approximation_unsafe(png_pixel* pixel)
{
  return
    color_channel_fix_approximation_unsafe(&pixel->red)   ||
    color_channel_fix_approximation_unsafe(&pixel->green) ||
    color_channel_fix_approximation_unsafe(&pixel->blue);
}

bool
png_pixel_fix_approximation(png_pixel* pixel)
{
  return
    pixel == NULL &&
    png_pixel_fix_approximation_unsafe(pixel);
}

bool
png_image_pixel_buffer_print_alpha_warning_to_stream_unsafe(png_image_pixel_buffer_t* buffer,
							    FILE* stream)
{
  bool has_alpha_channel = false;
  png_pixel* row;
  png_pixel* pixel;
  for(unsigned int line=0, column; line < buffer->get_height(); ++line)
    {
      row = (*buffer)[line].data();
      for(column=0; column < buffer->get_width(); ++column)
	{
	  pixel = &row[column];
	  if(pixel->alpha != 255)
	    {
	      fputs("Alpha channel is not managed, you should remove it.",
		    stream);
	      fputc('\n', stream);
	      has_alpha_channel = true;

	      pixel->alpha = 255;
	    }
	}
    }
  return has_alpha_channel;
}

bool
png_image_print_alpha_warning_to_stream_unsafe(png_image_t* img,
					       FILE* stream)
{
  return png_image_pixel_buffer_print_alpha_warning_to_stream_unsafe(&img->get_pixbuf(),
								     stream);
}

bool
png_image_print_alpha_warning_to_stderr_unsafe(png_image_t* img)
{
  return png_image_print_alpha_warning_to_stream_unsafe(img, stderr);
}

void
png_image_pixel_buffer_print_warnings_to_stream_unsafe(png_image_pixel_buffer_t* buffer,
						       FILE* stream)
{
  png_image_pixel_buffer_print_alpha_warning_to_stream_unsafe(buffer, stream);
  
  png_pixel* row;
  png_pixel* pixel;
  for(unsigned int line=0, column; line < buffer->get_height(); ++line)
    {
      row = (*buffer)[line].data();
      for(column=0; column < buffer->get_width(); ++column)
	{
	  pixel = &row[column];

	  if(png_pixel_fix_approximation_unsafe(pixel))
	    {
	      fputs("Pixel tolerated ", stream);
	      fprintf(stream,
		      "(line=%u/%u, column=%u/%u)",
		      line,   (uint) buffer->get_height(),
		      column, (uint) buffer->get_width());
	      fputc(' ', stream);
	      fputs(png_pixel_rgb_to_cstring(buffer->get_pixel(column, line)),
		    stream);
	      fputc('\n', stream);
	    }
	}
    }
}

void
png_image_print_warnings_to_stream_unsafe(png_image_t* img,
					  FILE* stream)
{
  png_image_pixel_buffer_print_warnings_to_stream_unsafe(&img->get_pixbuf(),
							 stream);
}

void
png_image_print_warnings_to_stderr_unsafe(png_image_t* img)
{
  png_image_print_warnings_to_stream_unsafe(img, stderr);
}

bool
png_image_pixel_buffer_to_map_2d_unsafe(const png_image_pixel_buffer_t* buffer,
					map_2d_char* map,
					FILE* error_stream)
{
  png_pixel known_colors[7] = {
    {  0,   0,   0, 255},
    {255, 255, 255, 255},
    {255, 255,   0, 255},
    {  0, 255,   0, 255},
    {  0,   0, 255, 255},
    {  0, 255, 255, 255},
    {255,   0, 255, 255}
  };
  map_tile color_to_tile[7] = {
    MAP_TILE_OUT,
    MAP_TILE_NORMAL,
    MAP_TILE_SAND,
    MAP_TILE_GOAL,
    MAP_TILE_START1,
    MAP_TILE_START2,
    MAP_TILE_START3
  };
  signed char known_color_index;
  
  map->nb_lines   = buffer->get_height();
  map->nb_columns = buffer->get_width();
  tab_2d_char_alloc_unsafe(map);
  
  const png_pixel* row;
  const png_pixel* pixel;
  for(unsigned int line=0, column, i; line < map->nb_lines; ++line)
    {
      row = (*buffer)[line].data();
      
      for(column=0; column < map->nb_columns; ++column)
	{
	  pixel = &row[column];

	  known_color_index = -1;
	  for(i=0; i < 7; ++i)
	    {
	      if(png_pixel_equals_unsafe(pixel, &known_colors[i]))
		{
		  known_color_index = i;
		  break;
		}
	    }
	  if(known_color_index < 0)
	    {
	      fputs("Unknow color ", error_stream);
	      fprintf(error_stream,
		      "(line=%u/%u, column=%u/%u)",
		      line,   map->nb_lines,
		      column, map->nb_columns);
	      fputc(' ', error_stream);
	      fputs(png_pixel_rgb_to_cstring_unsafe(pixel), error_stream);
	      fputc('\n', error_stream);
	      return false;
	    }
	  
	  tab_2d_char_set_element_value_unsafe(map,
					       line,
					       column,
					       color_to_tile[known_color_index]);
	}
    }
  return true;
}

bool
png_image_to_map_2d_unsafe(const png_image_t* img,
			   map_2d_char* map,
			   FILE* error_stream)
{
  return png_image_pixel_buffer_to_map_2d_unsafe(&img->get_pixbuf(),
						 map,
						 error_stream);
}

bool
png_image_path_to_map_2d_unsafe(const char* img_path,
				map_2d_char* map,
				FILE* error_stream)
{
  png_image_t img(img_path);
  png_image_print_warnings_to_stream_unsafe(&img, error_stream);
  return png_image_to_map_2d_unsafe(&img, map, error_stream);
}


#endif

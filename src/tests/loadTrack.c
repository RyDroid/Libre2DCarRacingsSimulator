/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "gp_map_io.h"
#include "arguments_management.h"
#include "io_functions.h"


bool
remove_starts_from_map_unsafe(unsigned int argc,
			      const char* argv[])
{
  bool answer = false;
  for(unsigned int i=0; i < argc; ++i)
    {
      if(cstring_equals(argv[i], "--remove-starts") ||
	 cstring_equals(argv[i], "--delete-starts"))
	{
	  answer = true;
	}
      else if(cstring_equals(argv[i], "--keep-starts"))
	{
	  answer = false;
	}
    }
  return answer;
}

int
main(int argc, char* argv[])
{
  if(argc == 1)
    {
      fprintf(stderr,
	      "usage: %s optional_args file_path_to_a_track_txt\n",
	      argv[0]);
      return EXIT_FAILURE;
    }

  /* Argument management with message and exit result */
  if (is_a_help_cstring_option_unsafe(argv[1]))
    {
      printf("usage: %s file_path_to_a_track_txt\n", argv[0]);
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--license") ||
     cstring_equals(argv[1], "--licence") ||
     cstring_equals(argv[1], "--copyright"))
    {
      puts("This program is under GNU Affero General Public License 3 "
           "as published by the Free Software Foundation, "
           "either version 3 of the License, "
           "or (at your option) any later version.");
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--author") ||
     cstring_equals(argv[1], "--auteur"))
    {
      puts("The author of this program is Nicola Spanti, "
           "also known as RyDroid.");
      return EXIT_SUCCESS;
    }
  
  gp_map map = gp_map_create_empty();
  bool remove_starts = remove_starts_from_map_unsafe(argc, argv);
  
  FILE* file = fopen_with_error_message_to_stderr_unsafe(argv[argc-1], "r");
  if(file == NULL)
    {
      return EXIT_FAILURE;
    }
  if(!gp_map_load_from_stream(&map, file, remove_starts))
    {
      fputs("Loading failure!", stderr);
      fputc('\n', stderr);
      fclose(file);
      return EXIT_FAILURE;
    }
  fclose(file);
  
  printf("## Cars (%u)\n", map.cars.size);
  gp_map_print_cars_to_stdout_as_an_ordered_list(&map);
  putchar('\n');
  putchar('\n');
  
  puts("## Map");
  printf("nbLines=%u, nbColumns=%u\n",
	 map.map.nb_lines,
	 map.map.nb_columns);
  gp_map_print_map_to_stdout_without_grid(&map);
  
  gp_map_destruct_unsafe(&map);
  
  return EXIT_SUCCESS;
}

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef ARGUMENTS_MANAGEMENT_H
#define ARGUMENTS_MANAGEMENT_H


#include "cstring_functions.h"


static inline
bool
is_a_short_cstring_option_unsafe(const char* arg)
{
  return
    arg[0] == '-' &&
    arg[1] != '-' && arg[1] != '\0' && arg[1] != '\n' &&
    arg[2] == '\0';
}

static inline
bool
is_a_long_cstring_option_unsafe(const char* arg)
{
  return
    arg[0] == '-' && arg[1] == '-' &&
    arg[2] != '\0' && arg[2] != '\n';
}

static inline
bool
is_a_help_cstring_option_unsafe(const char* arg)
{
  return
    arg[0] == '-'
    &&
    (((arg[1] == '?' ||
       arg[1] == 'h') &&
      arg[2] == '\0')
     ||
     (arg[1] == '-'  &&
      (cstring_equals(arg+2, "help")   ||
       cstring_equals(arg+2, "usage")  ||
       cstring_equals(arg+2, "man")    ||
       cstring_equals(arg+2, "manual") ||
       cstring_equals(arg+2, "info"))));
}

static inline
bool
is_a_help_cstring_option(const char* arg)
{
  return
    arg != NULL &&
    is_a_help_cstring_option_unsafe(arg);
}


#endif

/*
 * License Creative Commons 0 (like the public domain)
 * Use, study, hack and share!
 * Even if you are not force with copyleft to respect freedom of others, please do it.
 * For more informations : https://www.gnu.org/philosophy/free-sw.html
 * This program is provided with no warranty.
 * Names of contributors must not be used to endorse or promote products derived from this file without specific prior written permission.
 */


/**
 * @file
 * stdbool.h of C99 and C11 contains the same things.
 */


#ifndef BOOL_H
#define BOOL_H

/**
 * @brief Specifies whether bool, true and false are defined.
 */
#define	__bool_true_false_are_defined 1

#ifndef __cplusplus

#define false 0
#define true  1
#define FALSE 0
#define TRUE  1
typedef char bool;

#endif /* __cplusplus */

#endif /* BOOL_H */

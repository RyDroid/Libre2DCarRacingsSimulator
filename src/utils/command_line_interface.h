/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef COMMAND_LINE_INTERFACE_H
#define COMMAND_LINE_INTERFACE_H


#include <stdio.h>
#include "arguments_management.h"


void
one_line_action_helper_to_stream_unsafe(const char* action,
					const char* description,
					FILE* stream)
{
  fputs("  ", stream);
  fputs(action, stream);
  fputs(" - ", stream);
  fputs(description, stream);
  fputc('\n', stream);
}

void
one_line_option_helper_to_stream_unsafe(const char* option,
					const char* description,
					FILE* stream)
{
  fputs("  ", stream);
  fputs(option, stream);
  fputc(' ', stream);
  fputs(description, stream);
  fputc('\n', stream);
}

void
one_line_option_helper_to_stdout_unsafe(const char* option,
					const char* description)
{
  one_line_option_helper_to_stream_unsafe(option,
					  description,
					  stdout);
}


#endif

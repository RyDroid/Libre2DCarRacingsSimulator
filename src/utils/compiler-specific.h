/**
 * License Creative Commons 0 (like the public domain)
 * https://creativecommons.org/publicdomain/zero/1.0/
 * 
 * Use, study, hack and share!
 * Even if you are not force to respect freedom of others with copyleft, please do it.
 * For more informations: https://www.gnu.org/philosophy/free-sw.html
 * 
 * This file is provided with no warranty.
 * Names of contributors must not be used to endorse or promote products derived from this file without specific prior written permission.
 */


/**
 * @file
 * @brief Some compiler specifics things.
 */


#ifndef COMPILER_SPECIFIC_H
#define COMPILER_SPECIFIC_H


/**
 * @brief If we're not using GNU C, elide __attribute__
 */
#ifndef __GNUC__
#  define  __attribute__(x)  /* NOTHING */
#endif


#endif

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions to manage strings in C.
 */


#ifndef CSTRING_FUNCTIONS_H
#define CSTRING_FUNCTIONS_H


#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "bool.h"


/**
 * @brief Check if 2 strings are equal.
 * @param str1 A string
 * @param str2 An other string
 * @return True if the first string equals the other one, otherwise false
 */
static inline
bool
cstring_equals(const char* str1,
	       const char* str2)
{
  return strcmp(str1, str2) == 0;
}

static inline
bool
cstring_starts_with(const char* string,
		    const char* potential_prefix)
{
  return strstr(string, potential_prefix) == string;
}

bool
cstring_ends_with(const char* base,
		  const char* str)
{
    unsigned int blen = strlen(base);
    unsigned int slen = strlen(str);
    return blen >= slen && strcmp(base + blen - slen, str) == 0;
}

/**
 * @brief Reverse the order of characters in the string.
 * @param string String to reverse
 */
static inline
void
reverse_cstring(char* string)
{
  size_t length = strlen(string);
  size_t half_length = length / 2;
  size_t i=0;
  char tmp;
  
  while(i < half_length)
    {
      tmp = string[i];
      string[i] = string[length - i -1];
      string[length - i -1] = tmp;
      ++i;
    }
}

/**
 * @brief Returns a copy of a string.
 * @param string String to copy
 * @return A copy of a string
 */
static inline
char*
get_strcpy(const char* string)
{
  if(string == NULL)
    {
      return NULL;
    }
  
  char* new_string = (char*) malloc(sizeof(char) * (strlen(string) +1));
  strcpy(new_string, string);
  return new_string;
}

/**
 * @brief Returns a string reversed.
 * @param string String to reverse
 * @return A string reversed
 */
static inline
char*
get_reverse_cstring(const char* string)
{
  char* string_reversed = get_strcpy(string);
  reverse_cstring(string_reversed);
  return string_reversed;
}

/**
 * @brief Trim to the left a string with a character to strip.
 * @param string String to trim to the left
 * @param character_to_strip A character to strip
 */
static inline
void
cstring_ltrim(char* string,
	      char character_to_strip)
{
  if(string != NULL)
    {
      while(*string == character_to_strip)
	{
	  strcpy(string, string + 1);
	}
    }
}

/**
 * @brief Trim to the right a string with a character to strip.
 * @param string String to trim to the right
 * @param character_to_strip A character to strip
 */
static inline
void
cstring_rtrim(char* string,
	      char character_to_strip)
{
  if(string != NULL)
    {
      string += strlen(string) -1;
      while(*string == character_to_strip)
	{
	  *string = '\0';
	  --string;
	}
    }
}

/**
 * @brief Trim to the left and right a string with a character to strip.
 * @param string String to trim to the left and right
 * @param character_to_strip A character to strip
 */
static inline
void
cstring_trim(char* string,
	     char character_to_strip)
{
  cstring_ltrim(string, character_to_strip);
  cstring_rtrim(string, character_to_strip);
}

/**
 * @brief Returns the number of occurrences of a character in a string
 * @param string A string
 * @param character_to_strip A character to count
 * @return The number of occurrences of a character in a string
 */
static inline
unsigned int
get_nb_of_character_of_cstring(const char* string,
			       char character)
{
  if(string == NULL)
    {
      return 0;
    }
  
  unsigned int nb=0;
  while(*string)
    {
      if(*string == character)
	{
	  ++nb;
	}
      ++string;
    }
  return nb;
}

/**
 * @brief All characters of a string become lower cases.
 * @param string String to lowercase
 */
static inline
void
cstring_tolower(char* string)
{
  if(string != NULL)
    {
      while(*string)
	{
	  *string = tolower(*string);
	  ++string;
	}
    }
}

/**
 * @brief All characters of a string become upper cases.
 * @param string String to uppercase
 */
static inline
void
cstring_toupper(char* string)
{
  if(string != NULL)
    {
      while(*string)
	{
	  *string = toupper(*string);
	  ++string;
	}
    }
}

/**
 * @brief Returns true if a string contains only digits, otherwise false.
 * @param string A string
 * @return True if a string contains only digits, otherwise false
 */
static inline
bool
cstring_contains_only_digits(const char* string)
{
  if(string == NULL)
    {
      return false;
    }
  
  while(*string)
    {
      if(!isdigit(*string))
	{
	  return false;
	}
      ++string;
    }
  return true;
}

/**
 * @brief Returns true if a string is an integer, otherwise false.
 * @param string A string
 * @return True if a string is an integer, otherwise false
 */
static inline
bool
cstring_is_int(const char* string)
{
  return
    string != NULL &&
    (*string == '+' || *string == '-' || isdigit(*string)) &&
    cstring_contains_only_digits(++string);
}

/**
 * @brief Returns true if a string is an unsigned integer, otherwise false.
 * @param string A string
 * @return True if a string is an unsigned integer, otherwise false
 */
static inline
bool
cstring_is_uint(const char* string)
{
  return
    string != NULL &&
    (*string == '+' || isdigit(*string)) &&
    cstring_contains_only_digits(++string);
}


#endif

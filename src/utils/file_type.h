/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef FILE_TYPE_H
#define FILE_TYPE_H


#include "cstring_functions.h"


enum file_type
  {
    FILE_TYPE_UNDEFINED,
    
    /* Text */
    FILE_TYPE_PLAIN_TEXT,
    FILE_TYPE_MARKDOWN,
    FILE_TYPE_TEX,
    FILE_TYPE_HTML,
    
    /* Image */
    FILE_TYPE_PNG,
    FILE_TYPE_PPM,
    FILE_TYPE_WEBP,
    FILE_TYPE_JPEG,
    FILE_TYPE_SVG,
    
    /* Source code */
    FILE_TYPE_SHELL_SCRIPT,
    FILE_TYPE_C_HEADER,
    FILE_TYPE_CPP_HEADER,
    FILE_TYPE_FORTRAN,
    FILE_TYPE_ADA,
    FILE_TYPE_JAVASCRIPT,
    FILE_TYPE_JAVA,
    FILE_TYPE_SCLALA,
    FILE_TYPE_PYTHON,
    FILE_TYPE_PERL,
    FILE_TYPE_PHP,
    FILE_TYPE_OCTAVE,
    FILE_TYPE_R_SCRIPT,
    FILE_TYPE_LISP,
    FILE_TYPE_COMMON_LISP,
    FILE_TYPE_EMACS_LISP,
    FILE_TYPE_LUA,
    FILE_TYPE_GO,
    FILE_TYPE_RUST,
    FILE_TYPE_SWIFT,
    FILE_TYPE_COBOL,
    FILE_TYPE_ERLANG,
    
    /* Non plain-text document */
    FILE_TYPE_PDF,
    FILE_TYPE_PS,
    FILE_TYPE_OPEN_DOCUMENT,
    
    /* Data */
    FILE_TYPE_XML,
    FILE_TYPE_JSON,
    FILE_TYPE_CSV,
    
    /* Archive */
    FILE_TYPE_ZIP,
    FILE_TYPE_TAR,

    /* Data checking */
    FILE_TYPE_DTD,
    FILE_TYPE_XSD
  };

typedef enum file_type file_type;


static inline
bool
is_plain_text_type_cstring(const char* str)
{
  return
    cstring_equals(str, "txt")  ||
    cstring_equals(str, "TXT")  ||
    cstring_equals(str, "text") ||
    cstring_equals(str, "TEXT") ||
    cstring_equals(str, "plain-text");
}

static inline
bool
is_plain_text_file_path(const char* file_path)
{
  return
    cstring_ends_with(file_path, ".txt")  ||
    cstring_ends_with(file_path, ".TXT")  ||
    cstring_ends_with(file_path, ".text") ||
    cstring_ends_with(file_path, ".TEXT");
}

static inline
bool
is_png_type_cstring(const char* str)
{
  return
    cstring_equals(str, "png") ||
    cstring_equals(str, "PNG") ||
    cstring_equals(str, "PortableNetworkGraphics");
}

static inline
bool
is_png_file_path(const char* file_path)
{
  return
    cstring_ends_with(file_path, ".png") ||
    cstring_ends_with(file_path, ".PNG");
}

static inline
bool
is_ppm_type_cstring(const char* str)
{
  return
    cstring_equals(str, "ppm") ||
    cstring_equals(str, "PPM") ||
    cstring_equals(str, "PortablePixMap");
}

static inline
bool
is_ppm_file_path(const char* file_path)
{
  return
    cstring_ends_with(file_path, ".ppm") ||
    cstring_ends_with(file_path, ".PPM");
}

static inline
bool
is_webp_type_cstring(const char* str)
{
  return
    cstring_equals(str, "webp") ||
    cstring_equals(str, "WEBP") ||
    cstring_equals(str, "WebP");
}

static inline
bool
is_webp_file_path(const char* file_path)
{
  return
    cstring_ends_with(file_path, ".webp") ||
    cstring_ends_with(file_path, ".WEBP") ||
    cstring_ends_with(file_path, ".WebP");
}

static inline
bool
is_jpeg_type_cstring(const char* str)
{
  return
    cstring_equals(str, "jpg")  ||
    cstring_equals(str, "JPG")  ||
    cstring_equals(str, "jpeg") ||
    cstring_equals(str, "JPEG");
}

static inline
bool
is_jpeg_file_path(const char* file_path)
{
  return
    cstring_ends_with(file_path, ".jpg")  ||
    cstring_ends_with(file_path, ".JPG")  ||
    cstring_ends_with(file_path, ".jpeg") ||
    cstring_ends_with(file_path, ".JPEG");
}


#endif

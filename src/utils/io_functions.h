/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef IO_FUNCTIONS_H
#define IO_FUNCTIONS_H


#include <stdio.h>
#include "bool.h"


static inline
bool
file_exists(const char* file_path)
{
  FILE* file = fopen(file_path, "r");
  if(file == NULL)
    {
      return false;
    }
  fclose(file);
  return true;
}

static inline
FILE*
fopen_with_error_message_to_stream_unsafe(const char* file_path,
					  const char* mode,
					  FILE* error_stream)
{
  FILE* file = fopen(file_path, mode);
  if(file == NULL)
    {
      fprintf(error_stream,
	      "File %s does not exist or this program had been invoked by a user that has not the right to read it!",
	      file_path);
      fputc('\n', error_stream);
    }
  return file;
}

static inline
FILE*
fopen_with_error_message_to_stderr_unsafe(const char* file_path,
					  const char* mode)
{
  return fopen_with_error_message_to_stream_unsafe(file_path,
						   mode,
						   stderr);
}


#endif

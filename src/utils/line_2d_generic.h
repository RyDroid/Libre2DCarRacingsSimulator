/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Generic functions to manage a 2D line.
 */


#ifndef LINE_2D_GENERIC_H
#define LINE_2D_GENERIC_H


#include "position_2d_generic.h"
#include "pair_generic.h"


/**
 * @brief A pair of 2 2D positions of a specified type.
 * @param type A type
 */
#define DEFINE_LINE_2D_STRUCT(type)			\
  DEFINE_PAIR_STRUCT(position_2d_##type);		\
  typedef pair_position_2d_##type line_2d_##type;


/**
 * @brief Returns the length of a line in a 2D space of unsigned int.
 * @param line A line in a 2D space of unsigned int
 * @return The length of a line in a 2D space of unsigned int
 */
#define LINE_2D_LENGTH(line)					\
  DISTANCE_BETWEEN_2_POSITIONS_2D((line).first, (line).second);


#endif

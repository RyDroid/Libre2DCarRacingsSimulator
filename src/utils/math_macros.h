/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Macros related to math
 */


#ifndef MATH_MACROS_H
#define MATH_MACROS_H


/**
 * @brief Returns the minimum value.
 * @param value1 A value
 * @param value2 An other value
 * @return The minimum value
 */
#define MIN(value1, value2) ( (value1) < (value2) ? (value1) : (value2) )

/**
 * @brief Returns the maximum value.
 * @param value1 A value
 * @param value2 An other value
 * @return The maximum value
 */
#define MAX(value1, value2) ( (value1) > (value2) ? (value1) : (value2) )

/**
 * @brief Returns the argument multiply by itself.
 * @param x A number
 * @return The argument multiply by itself
 */
#define POW2(x) ( (x) * (x) )


#endif

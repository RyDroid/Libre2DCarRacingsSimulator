/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef NUMBER_FUNCTIONS_H
#define NUMBER_FUNCTIONS_H


/**
 * @brief Returns the number of digits of a unsigned integer.
 * @param nb A unsigned integer
 * @return Number of digits of a unsigned integer
 */
static inline
unsigned char
number_of_digits_of_unsigned_int(unsigned int nb)
{
  unsigned int count = 1;
  while(nb > 9)
    {
      nb /= 10;
      ++count;
    }
  return count;
}


#endif

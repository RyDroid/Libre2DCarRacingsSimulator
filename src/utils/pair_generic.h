/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions to manage a pair of the same type.
 * It is like std::pair<type, type> of C++.
 */


#ifndef PAIR_GENERIC_H
#define PAIR_GENERIC_H


/**
 * @brief Define a pair of specified type.
 * @param type A type
 */
#define DEFINE_PAIR_STRUCT(type)		\
  struct pair_##type				\
  {						\
    type first;					\
    type second;				\
  };						\
  typedef struct pair_##type pair_##type;

/**
 * @brief Swap the 2 elements of a pair.
 * @param pair A pair
 * @param type Type of the 2 elements of the pair
 */
#define PAIR_SWAP(pair, type)			\
  do						\
    {						\
      type tmp = (pair).first;			\
      (pair).first = (pair).second;		\
      (pair).second = tmp;			\
    } while (0);

/**
 * @brief Check if 2 pairs are equal.
 * @param pair1 A pair
 * @param pair2 An other pair
 * @return True if the 2 pairs are equal, otherwise false
 */
#define PAIR_EQUALS(pair1, pair2)		\
  (pair1).first  == (pair2).first &&		\
  (pair1).second == (pair2).second


#endif // PAIR_GENERIC_H

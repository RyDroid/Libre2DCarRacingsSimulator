/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef PNG_PIXEL_HPP
#define PNG_PIXEL_HPP


#include <png++/rgba_pixel.hpp>
#include <tuple>


typedef png::rgba_pixel png_pixel_rgba;
typedef png_pixel_rgba png_pixel;


bool
png_pixel_equals_unsafe(const png_pixel* a, const png_pixel* b)
{
  return
    a->red   == b->red   &&
    a->green == b->green &&
    a->blue  == b->blue  &&
    a->alpha == b->alpha;
}

bool
png_pixel_equals(const png_pixel* a, const png_pixel* b)
{
  return
    a == NULL || b == NULL
    ? false
    : png_pixel_equals_unsafe(a, b);
}

bool
operator==(const png_pixel& a, const png_pixel& b)
{
  return png_pixel_equals_unsafe(&a, &b);
}

bool
operator!=(const png_pixel& a, const png_pixel& b)
{
  return ! png_pixel_equals_unsafe(&a, &b);
}

bool
operator<(const png_pixel& a, const png_pixel& b)
{
  return std::tie(a.red, a.green, a.blue, a.alpha) < std::tie(b.red, b.green, b.blue, b.alpha);
}

bool
operator>(const png_pixel& a, const png_pixel& b)
{
  return std::tie(a.red, a.green, a.blue, a.alpha) > std::tie(b.red, b.green, b.blue, b.alpha);
}

/* see https://stackoverflow.com/questions/32746903/using-implicit-comparator-with-a-map-that-has-a-struct-as-key-in-c */
struct png_pixel_sort_default
{
    bool operator()(const png_pixel& a, const png_pixel& b) const
    {
        return a < b;
    }
};

char*
png_pixel_rgb_to_cstring_unsafe(const png_pixel* color)
{
  char* string = (char*) malloc(2 /* '[' + ']'*/ +
				3 * 3 +
				2 /* nb1 +','+ nb2 +','+ nb3 */ +
				1 /* '\0' */);
  sprintf(string, "[%3u,%3u,%3u]",
	  color->red, color->blue, color->green);
  return string;
}

char*
png_pixel_rgb_to_cstring(const png_pixel& color)
{
  return png_pixel_rgb_to_cstring_unsafe(&color);
}


#endif

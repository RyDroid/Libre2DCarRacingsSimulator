/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Generic functions to manage a 2D position.
 */


#ifndef POSITION_2D_GENERIC_H
#define POSITION_2D_GENERIC_H


#include <math.h>


/**
 * @brief Define a 2D position and size of specified type.
 * @param type A type
 */
#define DEFINE_POSITION_2D_STRUCT(type)			\
  struct position_2d_##type				\
  {							\
    type x;						\
    type y;						\
  };							\
  typedef struct position_2d_##type position_2d_##type; \
  typedef position_2d_##type size_2d_##type;

/**
 * @brief Returns true if 2 2D positions are equal, otherwise false.
 * @param position1 A first position
 * @param position2 A second position
 * @return True if 2 2D positions are equal, otherwise false
 */
#define POSITION_2D_EQUALS(position1, position2)	\
  ( (position1).x == (position2).x &&			\
    (position1).y == (position2).y )			\


/**
 * @brief Returns the distance between 2 2D positions.
 * @param pos1 A first position
 * @param pos2 A second position
 * @return The distance between 2 2D positions
 */
#define DISTANCE_BETWEEN_2_POSITIONS_2D(pos1, pos2)		\
  sqrt( ( (pos2).x - (pos1).x) * ((pos2).x - (pos1).x) +	\
	( (pos2).y - (pos1).y) * ((pos2).y - (pos1).y) )

/**
 * @brief Returns the geometric aera of a 2D size.
 * @param size A size
 * @return The geometric aera of a 2D size
 */
#define SIZE_2D_AREA(size)  ( (size).x * (size).y )


#endif // POSITION_2D_GENERIC_H

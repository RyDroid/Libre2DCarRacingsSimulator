/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Structure of 2D position with unsigned members and functions to manage it.
 */


#ifndef POSITION_2D_UINT_H
#define POSITION_2D_UINT_H


#include "bool.h"
#include "position_2d_generic.h"


typedef unsigned int uint;
DEFINE_POSITION_2D_STRUCT(uint)


/**
 * @brief Swap the x member and the y member of a position 2D of unsigned int.
 * @param position A 2D position of unsigned int
 */
static inline
void
position_2d_uint_swap(position_2d_uint position)
{
  unsigned int tmp = position.x;
  position.x = position.y;
  position.y = tmp;
}

/**
 * @brief Returns true if 2 2D positions of unsigned int are equal, otherwise false.
 * @param position1 A first position
 * @param position2 A second position
 * @return True if 2 2D positions of unsigned int are equal, otherwise false
 */
static inline
bool
position_2d_uint_equals(const position_2d_uint position1,
			const position_2d_uint position2)
{
  return POSITION_2D_EQUALS(position1, position2);
}

/**
 * @brief Returns the distance between 2 2D positions of unsigned int.
 * @param position1 A first position
 * @param position2 A second position
 * @return The distance between 2 2D positions of unsigned int
 */
static inline
double
distance_between_2_positions_2d_uint(const position_2d_uint position1,
                                     const position_2d_uint position2)
{
  return DISTANCE_BETWEEN_2_POSITIONS_2D(position1, position2);
}

/**
 * @brief Returns the geometric aera of a 2D uint size.
 * @param size A size
 * @return The geometric aera of a 2D uint size
 */
static inline
unsigned int
size_2d_uint_area(const size_2d_uint size)
{
  return SIZE_2D_AREA(size);
}


#endif

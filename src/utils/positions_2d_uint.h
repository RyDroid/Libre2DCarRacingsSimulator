/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef POSITIONS_2D_UINT_H
#define POSITIONS_2D_UINT_H


#include "position_2d_uint.h"
#include "table_of_generic_static.h"
#include "queue_singly_linked_generic_static.h"


DEFINE_TABLE_OF(position_2d_uint)


DEFINE_QUEUE_SINGLY_LINKED_GENERIC_STATIC(position_2d_uint)
typedef queue_singly_linked_node_of_position_2d_uint queue_node_of_position_2d_uint;
typedef queue_singly_linked_of_position_2d_uint queue_of_position_2d_uint;

static inline
queue_of_position_2d_uint
queue_of_position_2d_uint_create()
{
  queue_of_position_2d_uint queue = {NULL, NULL};
  return queue;
}

static inline
void
queue_of_position_2d_uint_destruct(queue_of_position_2d_uint* queue)
{
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_DESTRUCT(queue, position_2d_uint);
}

static inline
void
queue_of_position_2d_uint_push_value_unsafe(queue_of_position_2d_uint* queue,
					    const position_2d_uint* pos)
{
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_UNSAFE(queue,
							 position_2d_uint,
							 pos);
}

static inline
unsigned int
queue_of_position_2d_uint_size(const queue_of_position_2d_uint* queue)
{
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_SIZE_CODE(queue,
					       position_2d_uint);
}

static inline
position_2d_uint*
queue_of_position_2d_uint_to_table_with_size_unsafe(const queue_of_position_2d_uint* queue,
						    unsigned int size)
{
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_TO_TABLE_WITH_SIZE_CODE(queue,
							     size,
							     position_2d_uint);
}

static inline
table_of_position_2d_uint
queue_of_position_2d_uint_to_table(const queue_of_position_2d_uint* queue)
{
  table_of_position_2d_uint tab;
  tab.size = queue_of_position_2d_uint_size(queue);
  tab.elements = queue_of_position_2d_uint_to_table_with_size_unsafe(queue,
								      tab.size);
  return tab;
}


#endif

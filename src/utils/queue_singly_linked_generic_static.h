/*
 * Copyright (C) 2014-2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef QUEUE_SINGLY_LINKED_GENERIC_STATIC_H
#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_H


#include <stdlib.h>
#include "bool.h"


#define DEFINE_QUEUE_SINGLY_LINKED_NODE_GENERIC_STATIC(type)		\
  typedef struct queue_singly_linked_node_of_##type queue_singly_linked_node_of_##type;	\
  struct queue_singly_linked_node_of_##type				\
  {									\
    type value;								\
    queue_singly_linked_node_of_##type * next;				\
  };

#define DEFINE_QUEUE_SINGLY_LINKED_GENERIC_STATIC(type)			\
  DEFINE_QUEUE_SINGLY_LINKED_NODE_GENERIC_STATIC(type)			\
  struct queue_singly_linked_of_##type					\
  {									\
    queue_singly_linked_node_of_##type * first;				\
    queue_singly_linked_node_of_##type * last;				\
  };									\
  typedef struct queue_singly_linked_of_##type queue_singly_linked_of_##type;


#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY_UNSAFE(queue)	\
  ((queue)->first == NULL)

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY(queue)	\
  ((queue) != NULL &&						\
   QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY_UNSAFE(queue))

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_POP(queue, type)		\
  if(!QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY(queue))		\
    {									\
      queue_singly_linked_node_of_##type * tmp = (queue)->first->next;	\
      free((queue)->first);						\
      (queue)->first = tmp;						\
      if((queue)->first == NULL)					\
	{								\
	  (queue)->last = NULL;						\
	}								\
    }

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_DESTRUCT_UNSAFE(queue, type)	\
  while(!QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY_UNSAFE(queue))	\
    {									\
      QUEUE_SINGLY_LINKED_GENERIC_STATIC_POP(queue, type);		\
    }

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_DESTRUCT(queue, type)	\
  while(!QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY(queue))		\
    {									\
      QUEUE_SINGLY_LINKED_GENERIC_STATIC_POP(queue, type);		\
    }

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_AS_FIRST_UNSAFE(queue, \
									type, \
									ptr_on_value) \
  if(QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY_UNSAFE(queue))		\
    {									\
      (queue)->first = (queue_singly_linked_node_of_##type *) malloc(sizeof(queue_singly_linked_node_of_##type)); \
      (queue)->last = queue->first;					\
    }									\
  else									\
    {									\
      (queue)->last->next = (queue_singly_linked_node_of_##type *) malloc(sizeof(queue_singly_linked_node_of_##type)); \
      (queue)->last->next->next = (queue)->first;			\
      (queue)->first = (queue)->last->next;				\
    }									\
  (queue)->first->value = *(ptr_on_value);				\
  (queue)->last->next = NULL;

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_AS_FISRT(queue,	\
								 type,	\
								 ptr_on_value) \
  if((queue) != NULL && (ptr_on_value) != NULL)				\
    {									\
      QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_AS_FISRT_UNSAFE(queue, \
								      type, \
								      ptr_on_value); \
    }

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_AS_LAST_UNSAFE(queue, \
								       type, \
								       ptr_on_value) \
  if(QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY_UNSAFE(queue))		\
    {									\
      (queue)->first = (queue_singly_linked_node_of_##type *) malloc(sizeof(queue_singly_linked_node_of_##type)); \
      (queue)->last = queue->first;					\
    }									\
  else									\
    {									\
      (queue)->last->next = (queue_singly_linked_node_of_##type *) malloc(sizeof(queue_singly_linked_node_of_##type)); \
      (queue)->last = (queue)->last->next;				\
    }									\
  (queue)->last->value = *(ptr_on_value);				\
  (queue)->last->next = NULL;

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_UNSAFE(queue,	\
							       type,	\
							       ptr_on_value) \
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_AS_LAST_UNSAFE(queue,	\
								 type,	\
								 ptr_on_value)

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_AS_LAST(queue,	\
								type,	\
								ptr_on_value) \
  if((queue) != NULL && (ptr_on_value) != NULL)				\
    {									\
      QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_AS_LAST_UNSAFE(queue, \
								     type, \
								     ptr_on_value); \
    }

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER(queue,		\
							type,		\
							ptr_on_value)	\
  QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_AS_LAST(queue,	\
							  type,		\
							  ptr_on_value)

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_COPY_POINTER_UNSAFE(dest,	\
							       src,	\
							       type)	\
  if(queue_is_empty_unsafe(src))					\
    {									\
      (dest)->first = NULL;						\
      (dest)->last  = NULL;						\
    }									\
  else									\
    {									\
      queue_destruct_unsafe(dest);					\
      (dest)->first = (queue_singly_linked_node*) malloc(sizeof(queue_singly_linked_node)); \
      (dest)->first->value = (src)->first->value;			\
      (dest)->last = (dest)->first;					\
      queue_node_of_##type * tmp = (src)->first;			\
      while((tmp = tmp->next) != NULL)					\
	{								\
	  (dest)->last->next = (queue_singly_linked_node_of_##type *) malloc(sizeof(queue_singly_linked_node_of_##type)); \
	  (dest)->last->next->value = tmp->value;			\
	  (dest)->last = (dest)->last->next;				\
	}								\
      (dest)->last->next = NULL;					\
    }

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_COPY_POINTER(dest,		\
							src,		\
							type)		\
  if((dest) != NULL && (src) != NULL)					\
    {									\
      QUEUE_SINGLY_LINKED_GENERIC_STATIC_COPY_POINTER_UNSAFE(dest,	\
							     src,	\
							     type);	\
    }

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_ADD(destination,		\
					       source,			\
					       type)			\
  if(!QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY(destination) &&	\
     !QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY(source))		\
    {									\
      queue_singly_linked_node_of_##type * current = (source)->first;	\
      while(current != NULL)						\
	{								\
	  QUEUE_SINGLY_LINKED_GENERIC_STATIC_PUSH_POINTER_UNSAFE(destination, \
								 type,	\
								 &current->value); \
	  current = current->next;					\
	}								\
    }

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_SIZE_CODE(queue, type)	\
  if(QUEUE_SINGLY_LINKED_GENERIC_STATIC_IS_EMPTY(queue))		\
    {									\
      return 0;								\
    }									\
									\
  unsigned int nb = 1;							\
  queue_singly_linked_node_of_##type * current = (queue)->first;	\
  while((current = current->next) != NULL)				\
    {									\
      ++nb;								\
    }									\
  return nb;

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_EQUALS_CODE(q1, q2, type)	\
  if((q1) == (q2))							\
    {									\
      return true;							\
    }									\
  if( ((q1) == NULL && (q2) != NULL) ||					\
      ((q1) != NULL && (q2) == NULL) )					\
    {									\
      return false;							\
    }									\
									\
  queue_singly_linked_node_of_##type * current1 = (q1)->first;		\
  queue_singly_linked_node_of_##type * current2 = (q2)->first;		\
									\
  while(true)								\
    {									\
      if(current1 == NULL && current2 == NULL)				\
	{								\
	  return true;							\
	}								\
      if((current1 == NULL && current2 != NULL) ||			\
	 (current1 != NULL && current2 == NULL) ||			\
	 current1->value != current2->value)				\
	{								\
	  return false;							\
	}								\
									\
      current1 = current1->next;					\
      current2 = current2->next;					\
    }

#define QUEUE_SINGLY_LINKED_GENERIC_STATIC_TO_TABLE_WITH_SIZE_CODE(queue, \
								   size, \
								   type) \
  if(size < 1)								\
    {									\
      return NULL;							\
    }									\
									\
  unsigned int nb = 0;							\
  type * tab = (type *) malloc(size * sizeof(type));			\
  queue_singly_linked_node_of_##type * current = (queue)->first;	\
  do									\
    {									\
      tab[nb++] = current->value;					\
    }									\
  while((current = current->next) != NULL);				\
  return tab;


#endif

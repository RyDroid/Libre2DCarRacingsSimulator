/*
 * Copyright (C) 2014, 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef TAB_2D_CHAR_ESSENTIAL_H
#define TAB_2D_CHAR_ESSENTIAL_H


#include "tab_2d_generic_static.h"


DEFINE_TAB_2D_GENERIC_STATIC(char)


/**
 * @brief Returns true if the 2D table of char is init.
 * @param tab_2d A pointer of a 2D table of char
 * @return True if the 2D table of char is init, otherwise false.
 */
static inline
bool
tab_2d_char_is_init(const tab_2d_char* tab_2d)
{
  return TAB_2D_GENERIC_STATIC_POINTER_IS_INIT(tab_2d);
}

/**
 * @brief Free properly dynamic allocated memory of a not null pointer on a 2D table of char.
 * @param tab_2d A not null pointer of a 2D table of char
 */
static inline
void
tab_2d_char_free_unsafe(tab_2d_char* tab_2d)
{
  TAB_2D_GENERIC_STATIC_POINTER_FREE_UNSAFE(tab_2d);
}

/**
 * @brief Destructs properly a not null pointer on a 2D table of char.
 * @param tab_2d A not null pointer of a 2D table of char
 */
static inline
void
tab_2d_char_destruct_unsafe(tab_2d_char* tab_2d)
{
  TAB_2D_GENERIC_STATIC_POINTER_DESTRUCT_UNSAFE(tab_2d);
}

/**
 * @brief Destructs properly a 2D table of char.
 * @param tab_2d A pointer of a 2D table of char
 */
static inline
void
tab_2d_char_destruct(tab_2d_char* tab_2d)
{
  if(tab_2d != NULL)
    {
      tab_2d_char_destruct_unsafe(tab_2d);
    }
}

/**
 * @brief Allocate unsafely (NULL pointer and size) a 2D table of char.
 * @param tab_2d A pointer of a 2D table of char
 */
static inline
void
tab_2d_char_alloc_unsafe(tab_2d_char* tab_2d)
{
  tab_2d->tab = (char *) malloc(tab_2d->nb_lines * tab_2d->nb_columns);
}

/**
 * @brief Allocate unsafely (NULL pointer and size) a 2D table of char with null values.
 * @param tab_2d A pointer of a 2D table of char
 */
static inline
void
tab_2d_char_alloc_null_unsafe(tab_2d_char* tab_2d)
{
  TAB_2D_GENERIC_STATIC_POINTER_ALLOC_NULL_UNSAFE(tab_2d, char);
}

/**
 * @brief Allocate with checks a 2D table of char.
 * @param tab_2d A pointer of a 2D table of char
 * @return True if a non null allocation was possible, otherwise false.
 */
static inline
bool
tab_2d_char_alloc(tab_2d_char* tab_2d)
{
  if(tab_2d == NULL ||
     tab_2d->nb_lines == 0 ||
     tab_2d->nb_columns == 0)
    {
      return false;
    }
  
  tab_2d_char_free_unsafe(tab_2d);
  tab_2d_char_alloc_unsafe(tab_2d);
  return true;
}

/**
 * @brief Initialise the 2D table of char for the first time.
 * @param tab_2d A not null pointer of a 2D table of char
 * @param nb_lines Number of lines of the 2D table of char
 * @param nb_columns Number of columns of the 2D table of char
 */
static inline
void
tab_2d_char_init_one_time_only_unsafe(tab_2d_char* tab_2d,
				      unsigned int nb_lines,
				      unsigned int nb_columns)
{
  tab_2d->nb_lines   = nb_lines;
  tab_2d->nb_columns = nb_columns;
  tab_2d_char_alloc_unsafe(tab_2d);
}

/**
 * @brief Initialise the 2D table of char for the first time.
 * tab should be NULL.
 * @param tab_2d A pointer of a 2D table of char
 * @param nb_lines Number of lines of the 2D table of char
 * @param nb_columns Number of columns of the 2D table of char
 */
static inline
void
tab_2d_char_init_one_time_only(tab_2d_char* tab_2d,
			       unsigned int nb_lines,
			       unsigned int nb_columns)
{
  if(tab_2d != NULL)
    {
      tab_2d_char_init_one_time_only_unsafe(tab_2d, nb_lines, nb_columns);
    }
}

/**
 * @brief Initialise the 2D table of char for the first time with null values.
 * @param tab_2d A not null pointer of a 2D table of char
 * @param nb_lines Number of lines of the 2D table of char
 * @param nb_columns Number of columns of the 2D table of char
 */
static inline
void
tab_2d_char_init_null_one_time_only_unsafe(tab_2d_char* tab_2d,
					   unsigned int nb_lines,
					   unsigned int nb_columns)
{
  tab_2d->nb_lines   = nb_lines;
  tab_2d->nb_columns = nb_columns;
  tab_2d_char_alloc_null_unsafe(tab_2d);
}

/**
 * @brief Initialize the 2D table of char unsafely.
 * There is no check on pointers.
 * @param tab_2d A pointer of a 2D table of char
 * @param nb_lines Number of lines of the 2D table of char
 * @param nb_columns Number of columns of the 2D table of char
 */
static inline
void
tab_2d_char_init_unsafe(tab_2d_char* tab_2d,
			unsigned int nb_lines,
			unsigned int nb_columns)
{
  tab_2d_char_free_unsafe(tab_2d);
  tab_2d_char_init_one_time_only_unsafe(tab_2d, nb_lines, nb_columns);
}

/**
 * @brief Initialize the 2D table of char safely.
 * There are checks on pointer.
 * @param tab_2d A pointer of a 2D table of char
 * @param nb_lines Number of lines of the 2D table of char
 * @param nb_columns Number of columns of the 2D table of char
 */
static inline
void
tab_2d_char_init(tab_2d_char* tab_2d,
		 unsigned int nb_lines,
		 unsigned int nb_columns)
{
  if(tab_2d != NULL)
    {
      tab_2d_char_init_unsafe(tab_2d, nb_lines, nb_columns);
    }
}

/**
 * @brief Initialize the 2D table of char unsafely with null values.
 * There is no check on pointers.
 * @param tab_2d A pointer of a 2D table of char
 * @param nb_lines Number of lines of the 2D table of char
 * @param nb_columns Number of columns of the 2D table of char
 */
static inline
void
tab_2d_char_init_null_unsafe(tab_2d_char* tab_2d,
			     unsigned int nb_lines,
			     unsigned int nb_columns)
{
  tab_2d_char_free_unsafe(tab_2d);
  tab_2d_char_init_null_one_time_only_unsafe(tab_2d, nb_lines, nb_columns);
}

/**
 * @brief Create an empty 2D table of char.
 * @return An empty 2D table of char
 */
static inline
tab_2d_char
tab_2d_char_create_empty()
{
  tab_2d_char tab_2d = {NULL, 0, 0};
  return tab_2d;
}

/**
 * @brief Create a 2D table of char.
 * @param nb_lines Number of lines of the 2D table of char
 * @param nb_columns Number of columns of the 2D table of char
 * @return A 2D table of char
 */
static inline
tab_2d_char
tab_2d_char_create(unsigned int nb_lines,
                   unsigned int nb_columns)
{
  tab_2d_char tab_2d = {NULL, nb_lines, nb_columns};
  tab_2d_char_alloc_unsafe(&tab_2d);
  return tab_2d;
}

/**
 * @brief Create a 2D table of char with null values.
 * @param nb_lines Number of lines of the 2D table of char
 * @param nb_columns Number of columns of the 2D table of char
 * @return A 2D table of char with null values
 */
static inline
tab_2d_char
tab_2d_char_create_with_null_values(unsigned int nb_lines,
                                    unsigned int nb_columns)
{
  tab_2d_char tab_2d = {NULL, nb_lines, nb_columns};
  tab_2d_char_alloc_null_unsafe(&tab_2d);
  return tab_2d;
}

/**
 * @brief Returns true if a line of the 2D table of char is init.
 * @param tab_2d A pointer of a 2D table of char
 * @param line A line of the 2D table of char
 * @return True if a line of the 2D table of char exists, otherwise false
 */
static inline
bool
tab_2d_char_line_exists(const tab_2d_char* tab_2d,
			unsigned int line)
{
  return
    tab_2d != NULL &&
    tab_2d->tab != NULL &&
    line < tab_2d->nb_lines;
}

/**
 * @brief Returns true if a column of the 2D table of char is init.
 * @param tab_2d A pointer of a 2D table of char
 * @param column A column of the 2D table of char
 * @return True if a column of the 2D table of char exists, otherwise false
 */
static inline
bool
tab_2d_char_column_exists(const tab_2d_char* tab_2d,
			  unsigned int column)
{
  return
    tab_2d != NULL &&
    tab_2d->tab != NULL &&
    column < tab_2d->nb_columns;
}

/**
 * @brief Returns true if an element of the 2D table of char is init.
 * @param tab_2d A pointer of a 2D table of char
 * @param line A line of the 2D table of char
 * @param column A column of the 2D table of char
 * @return True if an element of the 2D table of char exists, otherwise false.
 */
static inline
bool
tab_2d_char_element_exists(const tab_2d_char* tab_2d,
			   unsigned int line,
			   unsigned int column)
{
  return
    tab_2d != NULL &&
    tab_2d->tab != NULL &&
    line < tab_2d->nb_lines &&
    column < tab_2d->nb_columns;
}

/**
 * @brief Returns the pointer of an element of a 2D table of char with no check.
 * There is no check of the existence of the tab_2d, the tab of the tab_2d, the line and the column.
 * It is useful for optimization, but seg fault and null pointer can occur.
 * @param tab_2d A not null pointer of a 2D table of char
 * @param line A line of the 2D table of char
 * @param column A column of the 2D table of char
 * @return The pointer of the element (line, column) of the 2D table of char
 */
static inline
char*
tab_2d_char_get_element_pointer_unsafe(const tab_2d_char* tab_2d,
				       unsigned int line,
				       unsigned int column)
{
  return TAB_2D_GENERIC_STATIC_POINTER_GET_ELEMENT_POINTER_UNSAFE(tab_2d,
								  line,
								  column,
								  char);
}

/**
 * @brief Returns the pointer of an element of a 2D table of char with checks.
 * @param tab_2d A pointer of a 2D table of char
 * @param line A line of the 2D table of char
 * @param column A column of the 2D table of char
 * @return The pointer of the element (line, column) of the 2D table of char
 */
static inline
char*
tab_2d_char_get_element_pointer(const tab_2d_char* tab_2d,
				unsigned int line,
				unsigned int column)
{
  return
    tab_2d_char_element_exists(tab_2d, line, column)
    ? tab_2d_char_get_element_pointer_unsafe(tab_2d, line, column)
    : NULL;
}

/**
 * @brief Returns the value of an element of a 2D table of char with no check.
 * @param tab_2d A not null pointer of a 2D table of char
 * @param line A line of the 2D table of char
 * @param column A column of the 2D table of char
 * @return The value of the element (line, column) of the 2D table of char
 */
static inline
char
tab_2d_char_get_element_value_unsafe(const tab_2d_char* tab_2d,
				     unsigned int line,
				     unsigned int column)
{
  return TAB_2D_GENERIC_STATIC_POINTER_GET_ELEMENT_VALUE_UNSAFE(tab_2d, line, column, char);
}

/**
 * @brief Returns the value of an element of a 2D table of char with checks.
 * @param tab_2d A pointer of a 2D table of char
 * @param line A line of the 2D table of char
 * @param column A column of the 2D table of char
 * @return The value of the element (line, column) of the 2D table of char
 */
static inline
char
tab_2d_char_get_element_value(const tab_2d_char* tab_2d,
			      unsigned int line,
			      unsigned int column)
{
  char * pointer = tab_2d_char_get_element_pointer(tab_2d, line, column);
  return pointer == NULL ? '\0' : *pointer;
}

/**
 * @brief Set the value of the element (line, column) of a 2D table of char with no check.
 * @param tab_2d A pointer of a 2D table of char
 * @param line A line of the 2D table of char
 * @param column A column of the 2D table of char
 * @param value New value of the element
 */
static inline
void
tab_2d_char_set_element_value_unsafe(tab_2d_char* tab_2d,
				     unsigned int line,
				     unsigned int column,
				     char value)
{
  TAB_2D_GENERIC_STATIC_POINTER_SET_ELEMENT_VALUE_UNSAFE(tab_2d, line, column, value, char);
}

/**
 * @brief Set the value of the element (line, column) of a 2D table of char with checks.
 * @param tab_2d A pointer of a 2D table of char
 * @param line A line of the 2D table of char
 * @param column A column of the 2D table of char
 * @param value New value of the element
 */
static inline
void
tab_2d_char_set_element_value(tab_2d_char* tab_2d,
			      unsigned int line,
			      unsigned int column,
			      char value)
{
  char * pointer = tab_2d_char_get_element_pointer(tab_2d, line, column);
  if(pointer != NULL)
    {
      *pointer = value;
    }
}

static inline
void
tab_2d_char_set_nb_lines_unsafe(tab_2d_char* tab_2d,
                                unsigned int value)
{
  if(tab_2d->nb_lines != value)
    {
      tab_2d->nb_lines = value;
      tab_2d_char_free_unsafe(tab_2d);
    }
}

static inline
void
tab_2d_char_set_nb_columns_unsafe(tab_2d_char* tab_2d,
                                  unsigned int value)
{
  if(tab_2d->nb_columns != value)
    {
      tab_2d->nb_columns = value;
      tab_2d_char_free_unsafe(tab_2d);
    }
}


#endif

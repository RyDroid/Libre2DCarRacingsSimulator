/*
 * Copyright (C) 2014-2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "tab_2d_char_plus.h"


bool
tab_2d_char_swap_element(tab_2d_char* tab_2d,
			 unsigned int line1, unsigned int column1,
			 unsigned int line2, unsigned int column2)
{
  if(!tab_2d_char_is_init(tab_2d) ||
     !tab_2d_char_element_exists(tab_2d, line1, column1) ||
     !tab_2d_char_element_exists(tab_2d, line2, column2))
    {
      return false;
    }
  
  char tmp = tab_2d_char_get_element_value(tab_2d, line1, column1);
  tab_2d_char_set_element_value(tab_2d, line1, column1,
				tab_2d_char_get_element_value(tab_2d,
							      line2, column2));
  tab_2d_char_set_element_value(tab_2d, line2, column2, tmp);
  return true;
}

void
tab_2d_char_fill_with_const(tab_2d_char* tab_2d, char value_for_filling)
{
  if(tab_2d_char_is_init(tab_2d))
    {
      for(unsigned int line=0, column; line < tab_2d->nb_lines; ++line)
	{
	  for(column=0; column < tab_2d->nb_columns; ++column)
	    {
	      tab_2d_char_set_element_value_unsafe(tab_2d, line, column,
						   value_for_filling);
	    }
	}
    }
}

void
tab_2d_char_replace(tab_2d_char* tab_2d,
		    char value_to_replace, char new_value)
{
  if(tab_2d_char_is_init(tab_2d))
    {
      for(unsigned int line=0, column; line < tab_2d->nb_lines; ++line)
	{
	  for(column=0; column < tab_2d->nb_columns; ++column)
	    {
	      if(tab_2d_char_get_element_value_unsafe(tab_2d, line, column)
		 == value_to_replace)
		{
		  tab_2d_char_set_element_value_unsafe(tab_2d, line, column,
						       new_value);
		}
	    } 
	}
    }
}

unsigned int
tab_2d_char_nb_occurrences_of_a_value(const tab_2d_char* tab_2d,
				      char value)
{
  unsigned int nb = 0;
  for(unsigned int line=0, column; line < tab_2d->nb_lines; ++line)
    {
      for(column=0; column < tab_2d->nb_columns; ++column)
	{
	  if(tab_2d_char_get_element_value_unsafe(tab_2d,
						  line,
						  column) == value)
	    {
	      ++nb;
	    }
	}
    }
  return nb;
}

#if !defined(__cplusplus) && __STDC_VERSION__ >= 199901L
void
tab_2d_char_copy_unsafe(tab_2d_char* restrict destination,
			const tab_2d_char* restrict source)
#else
void
tab_2d_char_copy_unsafe(tab_2d_char* destination,
                        const tab_2d_char* source)
#endif
{
  destination->nb_lines   = source->nb_lines;
  destination->nb_columns = source->nb_columns;
  if(tab_2d_char_alloc(destination))
    {
      memcpy(destination,
	     source,
	     source->nb_lines * source->nb_columns);
    }
}

void
tab_2d_char_copy(tab_2d_char* destination,
		 const tab_2d_char* source)
{
  if(destination != source)
    {
      tab_2d_char_copy_unsafe(destination, source);
    }
}

tab_2d_char
tab_2d_char_get_copy_unsafe(const tab_2d_char* source)
{
  tab_2d_char tab_2d_copy = tab_2d_char_create(source->nb_lines,
					       source->nb_columns);
  tab_2d_char_copy(&tab_2d_copy, source);
  return tab_2d_copy;
}

tab_2d_char
tab_2d_char_get_copy(const tab_2d_char* source)
{
  return
    source == NULL
    ? tab_2d_char_create(0, 0)
    : tab_2d_char_get_copy_unsafe(source);
}

char*
tab_2d_char_get_line_copy(const tab_2d_char* tab_2d, unsigned int line)
{
  char* line_copy = (char*) malloc(sizeof(char) * (tab_2d->nb_columns +1));
  unsigned int i;
  for(i=0; i < tab_2d->nb_columns; ++i)
    {
      line_copy[i] = tab_2d_char_get_element_value_unsafe(tab_2d, line, i);
    }
  line_copy[i] = '\0';
  return line_copy;
}

char*
tab_2d_char_get_column_copy(const tab_2d_char* tab_2d, unsigned int column)
{
  char* column_copy = (char*) malloc(sizeof(char) * (tab_2d->nb_lines +1));
  unsigned int i;
  for(i=0; i < tab_2d->nb_lines; ++i)
    {
      column_copy[i] = tab_2d_char_get_element_value_unsafe(tab_2d, i, column);
    }
  column_copy[i] = '\0';
  return column_copy;
}

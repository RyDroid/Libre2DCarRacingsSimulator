/**
 * Copyright (C) 2014-2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 */


#ifndef TAB_2D_CHAR_PLUS_H
#define TAB_2D_CHAR_PLUS_H


#include <string.h>
#include "tab_2d_char_essential.h"


/**
 * Swap value of an element [line1, column1] with an other element [line2, column2] of a 2D table of char.
 * @param tab_2d A pointer of a 2D table of char
 * @param line1 A first line of the 2D table of char
 * @param column1 A first column of the 2D table of char
 * @param line2 A second line of the 2D table of char
 * @param column2 A second column of the 2D table of char
 * @return succeed True if the swap goes well, otherwise false.
 */
bool
tab_2d_char_swap_element(tab_2d_char* tab_2d,
			 unsigned int line1, unsigned int column1,
			 unsigned int line2, unsigned int column2);

/**
 * Fill a 2D table of char with a constant.
 * @param tab_2d A pointer of a 2D table of char
 * @param value_for_filling A value for filling
 */
void
tab_2d_char_fill_with_const(tab_2d_char* tab_2d, char value_for_filling);

/**
 * Replace a character by an other one in a 2D table of char.
 * @param tab_2d A pointer of a 2D table of char
 * @param value_to_replace A char value to replace
 * @param new_value A new value to replace the old one
 */
void
tab_2d_char_replace(tab_2d_char* tab_2d,
		    char value_to_replace, char new_value);

unsigned int
tab_2d_char_nb_occurrences_of_a_value(const tab_2d_char* tab_2d,
				      char value);

/**
 * Copy a table of 2D char in an other one.
 * @param destination A destination table 2D of char
 * @param source A source table 2D of char different of the desitination
 */

#if !defined(__cplusplus) && __STDC_VERSION__ >= 199901L
void
tab_2d_char_copy_unsafe(tab_2d_char* restrict destination,
			const tab_2d_char* restrict source);
#else
void
tab_2d_char_copy_unsafe(tab_2d_char* destination,
			const tab_2d_char* source);
#endif

/**
 * Copy a table of 2D char in an other one.
 * @param destination A destination table 2D of char
 * @param source A source table 2D of char
 */
void
tab_2d_char_copy(tab_2d_char* destination,
		 const tab_2d_char* source);

/**
 * Returns a copy of a not null pointer on table 2D of char.
 * @param source A source table 2D of char (!= NULL)
 * @return A copy of the source table 2D of char
 */
tab_2d_char
tab_2d_char_get_copy_unsafe(const tab_2d_char* source);

/**
 * Returns a copy of a table 2D of char.
 * @param source A source table 2D of char
 * @return A copy of the source table 2D of char
 */
tab_2d_char
tab_2d_char_get_copy(const tab_2d_char* source);

/**
 * Returns a copy of line of a table 2D of char.
 * @param tab A pointer of a 2D table of char
 * @param line Line to get a copy
 * @return A copy of a specified line of a table 2D of char
 */
char*
tab_2d_char_get_line_copy(const tab_2d_char* tab_2d, unsigned int line);

/**
 * Returns a copy of column of a table 2D of char.
 * @param tab A pointer of a 2D table of char
 * @param line Column to get a copy
 * @return A copy of a specified column of a table 2D of char
 */
char*
tab_2d_char_get_column_copy(const tab_2d_char* tab_2d, unsigned int column);


#endif

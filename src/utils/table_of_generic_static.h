/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Generic functions to manage a 1D table of something.
 */


#ifndef TABLE_OF_GENERIC_STATIC_H
#define TABLE_OF_GENERIC_STATIC_H


#include <stdlib.h>


/**
 * @brief Define a struct type table_of_type with 2 members: size (unsigned int) and elements (type*).
 * @param type Type of the elements of the defined struct
 */
#define DEFINE_TABLE_OF(type)				\
  struct table_of_##type				\
  {							\
    type * elements;					\
    unsigned int size;					\
  };							\
  typedef struct table_of_##type table_of_##type;

/**
 * @brief Define a struct type table_tiny_of_type with 2 members: size (unsigned char) and elements (type*).
 */
#define DEFINE_TABLE_TINY_OF(type)				\
  struct table_tiny_of_##type					\
  {								\
    type * elements;						\
    unsigned char size;						\
  };								\
  typedef struct table_tiny_of_##type table_tiny_of_##type;

/**
 * @brief Free properly dynamic allocated memory of a not null pointer on a table of something.
 * @param tab A not null pointer of a table of something
 */
#define TABLE_OF_GENERIC_STATIC_POINTER_FREE_UNSAFE(tab)	\
  if((tab)->elements != NULL)					\
    {								\
      free((tab)->elements);					\
      (tab)->elements = NULL;					\
    }

/**
 * @brief Destructs properly a not null pointer on a table of something.
 * @param tab A not null pointer of a table of something
 */
#define TABLE_OF_GENERIC_STATIC_POINTER_DESTRUCT_UNSAFE(tab)	\
  do								\
    {								\
      TABLE_OF_GENERIC_STATIC_POINTER_FREE_UNSAFE(tab);		\
      (tab)->size = 0;						\
    }								\
  while(false);


#endif

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief A timer type and functions related to it.
 */


#ifndef TIMER_H
#define TIMER_H


#include <time.h>


/**
 * @brief A timer type.
 */
typedef clock_t timer;


static inline
timer
get_timer_now()
{
  return clock();
}

/**
 * @brief Returns the difference between 2 timers.
 * @param last_timer The last timer
 * @param first_timer The first timer
 * @return The difference between 2 timers
 */
static inline
timer
get_diff_2_timers(const timer last_timer,
		  const timer first_timer)
{
  return last_timer - first_timer;
}

/**
 * @brief Returns the difference between now and a timer.
 * @param time A time to compare with now
 * @return The difference between now and a timer
 */
static inline
timer
get_diff_now_and_timer(const timer time)
{
  return get_diff_2_timers(get_timer_now(), time);
}

/**
 * @brief Returns the difference in seconds between now and a timer.
 * @param time A time to compare with now
 * @return The difference in seconds between now and a timer
 */
static inline
double
get_nb_seconds_between_timer_and_now(const timer time)
{
  return ((double) get_diff_now_and_timer(time)) / CLOCKS_PER_SEC;
}

/**
 * @brief Returns the difference in micro-seconds between now and a timer.
 * @param time A time to compare with now
 * @return The difference in micro-seconds between now and a timer
 */
static inline
double
get_nb_ms_between_timer_and_now(const timer time)
{
  return 1000 * get_nb_seconds_between_timer_and_now(time);
}


#endif
